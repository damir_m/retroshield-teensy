#ifndef _TERMINAL_H
#define _TERMINAL_H

// ; Fake terminal control in ROM area
#define io_area	0xfff0
#define io_cls	(io_area + 0)	// ; clear terminal window
#define io_putc	(io_area + 1)	// ; put char
#define io_putr	(io_area + 2)	// ; put raw char (doesn't interpret CR/LF)
#define io_puth	(io_area + 3)	// ; put as hex number
#define io_getc	(io_area + 4)	// ; get char

#endif // _TERMINAL_H