#ifndef _M6850_H
#define _M6850_H

////////////////////////////////////////////////////////////////////
// 6850 ACIA Implementation
//
////////////////////////////////////////////////////////////////////

#define M6850_CTRL_STAT_1 (0x10040)
#define M6850_TX_RX_1     (0x10042)

byte m6850_regTXD_1   = 0x00;
byte m6850_regRXD_1   = 0x00;
byte m6850_regCTRL_1  = 0b00010100;
byte m6850_regSTAT_1  = 0b00000110;

#define M6850_CTRL_STAT_2 (0x10041)
#define M6850_TX_RX_2     (0x10043)

byte m6850_regTXD_2   = 0x00;
byte m6850_regRXD_2   = 0x00;
byte m6850_regCTRL_2  = 0b00010100;
byte m6850_regSTAT_2  = 0b00000110;

#endif