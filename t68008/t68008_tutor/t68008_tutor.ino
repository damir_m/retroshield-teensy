////////////////////////////////////////////////////////////////////
// RetroShield 68008 for Arduino Mega/Teensy 3.5/3.6/4.1
//
// 2024/02/22
// Version 0.1

// The MIT License (MIT)

// Copyright (c) 2019 Erturk Kocalar, 8Bitforce.com

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// Date         Comments                                            Author
// -----------------------------------------------------------------------------
// 11/08/2024   Bring-up on Teensy.                                 Erturk

////////////////////////////////////////////////////////////////////
// Options
//   outputDEBUG: Print memory access debugging messages.
////////////////////////////////////////////////////////////////////
#define outputDEBUG 0

////////////////////////////////////////////////////////////////////
// BOARD DEFINITIONS
////////////////////////////////////////////////////////////////////

#include "memorymap.h"      // Memory Map (ROM, RAM, PERIPHERALS)
#include "portmap.h"        // Pin mapping to cpu
#include "setuphold.h"      // Delays required to meet setup/hold
#include "buttons.h"        // Functions to read 2 buttons on teensy adapter board
#include "m6850.h"          // 6850 Serial

// HDD LED light-up on serial output
#define LED_TIMEOUT_MS  50
unsigned long           LED_HDD_timeout;

////////////////////////////////////////////////////////////////////
// HOW TO SET 68008 OPERATING FREQUENCY
//
// There are two ways to clock  68008 on Retroshield 68008 Rev A:
// 1) Clock is supplied by Teensy. [Default]
//    We can adjust it in software, which is nice.
//    You can specify using M68008_CLK_FREQ below.
//    I tested up to 8 ~ 10 MHz.
// 2) Onboard crystal oscillator chip. [Optional]
//    Remember to modify the solder jumpers to select XTAL OSC output
//    instead of Teensy clock output. See documentation for details. 
////////////////////////////////////////////////////////////////////

#define M68008_CLK_FREQ   (8000000) // Hz

inline __attribute__((always_inline))
void m68_clock_start()
{
  #if ((ARDUINO_TEENSY35 || ARDUINO_TEENSY36) || (ARDUINO_TEENSY41))
    analogWriteFrequency(uP_MEGA_CLK, M68008_CLK_FREQ);
    analogWrite(uP_MEGA_CLK, 128);                        // duty-cycle = 50%
  #elif (ARDUINO_AVR_MEGA2560)
    pinMode(uP_MEGA_CLK, OUTPUT); 

    // set up Timer 5 (pin46, PL3, connected to OC5A)
    TCCR5A = bit (COM5A0); // |                   // toggle OC1A on Compare Match
            // bit (COM5B1) | bit (COM5B0) /    // Set OC5B on compare match
            // bit (COM5C1) | bit (COM5C0);     // set OC5C on compare match
    TCCR5B = bit (WGM52) | bit (CS50);   // CTC, no prescaling

    // Arduino Mega runs off of 16MHz so granularity is not great.

    // OCR5A =  0;   // 7.946MHz
    // OCR5A =  1;   // 3.973MHz
    // OCR5A =  2;   // 2.649MHz
    // OCR5A =  3;   // 1.986MHz
    OCR5A =  4;
  #endif
}

inline __attribute__((always_inline))
void m68_clock_stop()
{
  #if ((ARDUINO_TEENSY35 || ARDUINO_TEENSY36) || (ARDUINO_TEENSY41))
    analogWriteFrequency(uP_MEGA_CLK, M68008_CLK_FREQ);
    analogWrite(uP_MEGA_CLK, 0);                          // duty cycle = 0%; output low indefinitely
  #elif (ARDUINO_AVR_MEGA2560)
    analogWrite(uP_MEGA_CLK, 0);
  #endif
}

void uP_assert_reset()
{
  // Reset/Halt are bidirectional for 68008. It is pulled-up on the board. We drive it low only.
  pinMode(uP_RESET_N,       OUTPUT);    digitalWriteFast(uP_RESET_N,      LOW);    
  pinMode(uP_HALT_N,        OUTPUT);    digitalWriteFast(uP_HALT_N,       LOW); 

  // Drive RESET conditions
  digitalWriteFast(uP_IPL2_0_N,   HIGH);  
  digitalWriteFast(uP_IPL1_N,     HIGH);       
  digitalWriteFast(uP_BERR_N,     HIGH);

  digitalWriteFast(uP_DBUS_EN,     LOW);
  digitalWriteFast(uP_PULSE_DTACK, LOW);

  digitalWriteFast(uP_PEEK_ADDR_L, HIGH);  
  digitalWriteFast(uP_PEEK_ADDR_M, HIGH);    
  digitalWriteFast(uP_PEEK_ADDR_H, HIGH);    
  
  digitalWriteFast(LED_HDD,       HIGH);
  LED_HDD_timeout = 0;
}

void uP_release_reset()
{
  // Reset/Halt are bidirectional for 68008. It is pulled-up on the board. We drive it low only.

  digitalWriteFast(uP_RESET_N,  HIGH);  BUFFER_DELAY(); pinMode(uP_RESET_N, INPUT);
  digitalWriteFast(uP_HALT_N,   HIGH);  BUFFER_DELAY(); pinMode(uP_HALT_N,  INPUT);
}

void uP_reset_TWICE()
{
  uP_assert_reset();
  delay(100);
  uP_release_reset();
  delay(100);
  uP_assert_reset();      // Need 2nd reset to reset two FF's; require DS 0->1 to reset.
  delay(100);
  uP_release_reset();
  delay(100);
}

void uP_init()
{
  // xtal_osc_stop()
  pinMode(uP_MEGA_CLK,      OUTPUT);  digitalWriteFast(uP_MEGA_CLK,     LOW);   // - stop clock oscillator
  pinMode(uP_RESET_N,       OUTPUT);  digitalWriteFast(uP_RESET_N,      LOW);

  // These are active-log signals. L/M/H correspond to 8bit portions of A20..A0.
  pinMode(uP_PEEK_ADDR_L,   OUTPUT);  digitalWriteFast(uP_PEEK_ADDR_L,  HIGH);
  pinMode(uP_PEEK_ADDR_M,   OUTPUT);  digitalWriteFast(uP_PEEK_ADDR_M,  HIGH);    
  pinMode(uP_PEEK_ADDR_H,   OUTPUT);  digitalWriteFast(uP_PEEK_ADDR_H,  HIGH);

  pinMode(uP_HALT_N,        OUTPUT);  digitalWriteFast(uP_HALT_N,       LOW);
  pinMode(uP_IPL2_0_N,      OUTPUT);  digitalWriteFast(uP_IPL2_0_N,     HIGH); 
  pinMode(uP_IPL1_N,        OUTPUT);  digitalWriteFast(uP_IPL1_N,       HIGH);      
  pinMode(uP_BERR_N,        OUTPUT);  digitalWriteFast(uP_BERR_N,       HIGH);

  pinMode(uP_MEGA_AS_INV_N, INPUT);
  pinMode(uP_MEGA_DS_INV,   INPUT);
  pinMode(uP_MEGA_RW_INV,   INPUT);

  pinMode(uP_DBUS_EN,       OUTPUT);  digitalWriteFast(uP_DBUS_EN,      LOW);
  pinMode(uP_PULSE_DTACK,   OUTPUT);  digitalWriteFast(uP_PULSE_DTACK,  LOW);

  pinMode(LED_HDD,          OUTPUT);  digitalWriteFast(LED_HDD, HIGH); // active-low; turn off LED

  // Set directions for ADDR & DATA Bus.
  configure_PINMODE_ADDR();
  configure_PINMODE_DATA();

  uP_assert_reset();

  // Start oscillator
  m68_clock_start();
}

inline __attribute__((always_inline))
unsigned long read_addr20()
{
  unsigned long ADDR_L, ADDR_M, ADDR_H;

  digitalWriteFast(uP_PEEK_ADDR_L, LOW);    BUFFER_DELAY();  
    ADDR_L = ADDR8();           
  digitalWriteFast(uP_PEEK_ADDR_L, HIGH);   BUFFER_DELAY();
  digitalWriteFast(uP_PEEK_ADDR_M, LOW);    BUFFER_DELAY();  
    ADDR_M = ADDR8();           
  digitalWriteFast(uP_PEEK_ADDR_M, HIGH);   BUFFER_DELAY();
  digitalWriteFast(uP_PEEK_ADDR_H, LOW);    BUFFER_DELAY();  
    ADDR_H = (ADDR8() & 0x0F); 
  digitalWriteFast(uP_PEEK_ADDR_H, HIGH);   BUFFER_DELAY();

  return ((ADDR_H << 16) | (ADDR_M << 8) | (ADDR_L));
}

inline __attribute__((always_inline))
byte read_fc210()
{
  byte FC210;

  digitalWriteFast(uP_PEEK_ADDR_H, LOW);    BUFFER_DELAY();  
    FC210  = (ADDR8() & 0xF0) >> 4; 
  digitalWriteFast(uP_PEEK_ADDR_H, HIGH);   BUFFER_DELAY();

  return FC210;
}

void board_init()
{
  // Initialize supervisor stack & reset pointers
  //   copy from ROM to RAM:

  int i;
  for (i = 0; i <= 7; i++)
    RAM1[i] = ROM[i];
}


////////////////////////////////////////////////////////////////////
// Processor Control Loop
////////////////////////////////////////////////////////////////////

unsigned long uP_ADDR;
byte          uP_DATA;
byte          DATA_OUT;
byte          DATA_IN;

byte          uP_FC210;
#define uP_FUNC_RSV000     0b000
#define uP_FUNC_USERDATA   0b001
#define uP_FUNC_USERPROG   0b010
#define uP_FUNC_RSV011     0b011
#define uP_FUNC_RSV100     0b100
#define uP_FUNC_SUPERDATA  0b101
#define uP_FUNC_SUPERPROG  0b110
#define uP_FUNC_INT_ACK    0b111

bool          prev_RESET_N = true;
bool          prev_HALT_N  = true;

char          tmp[200];      // for debug sprintf buffer

inline __attribute__((always_inline))
void cpu_tick()
{    
  if (!digitalReadFast(uP_RESET_N) && prev_RESET_N)         // falling edge
  {
    // 68008 generated reset.
    Serial.println("INFO: 68008 asserted RESET.");

    // Deactivate interrupts.
    digitalWriteFast(uP_IPL2_0_N,     HIGH); 
    digitalWriteFast(uP_IPL1_N,       HIGH);      
    digitalWriteFast(uP_BERR_N,       HIGH);

    // Reset devices.
    board_init();
  }
  else
  if (!digitalReadFast(uP_HALT_N) && prev_HALT_N)         // falling edge
  {
    // 68008 generated reset.
    Serial.println("INFO: 68008 asserted HALT.");
  }
  else
  if (digitalReadFast(uP_MEGA_DS_INV))
  {
    //////////////////////////////
    // Address bus
    //////////////////////////////
    uP_FC210  = read_fc210();
    uP_ADDR   = read_addr20();

    if (outputDEBUG)
    {
      sprintf(tmp, "ADDR: \"%08lx\"", uP_ADDR);
      Serial.write(tmp);
    }

    //////////////////////////////
    // Data Transaction - READ or WRITE or INT-ACK
    //////////////////////////////

    // Pulse DBUS_EN to arm data buffer chip (U7) once
    digitalWriteFast(uP_DBUS_EN, HIGH); DELAY_UNIT_50NS(); digitalWriteFast(uP_DBUS_EN, LOW);

    //////////////////////////////
    // Interrupt ACK
    //////////////////////////////
    if (uP_FC210 == uP_FUNC_INT_ACK)
    {
      if (outputDEBUG) { Serial.print(" INT-ACK "); }

      // 68008 puts interrupt level number ack'ed on A3/A2/A1

      byte level  = (uP_ADDR & 0b1110)>>1;
      byte vector = 0x18;

      // Deassert external int pins.
      switch(level)
      {
        case 0b111: digitalWriteFast(uP_IPL2_0_N, HIGH); digitalWriteFast(uP_IPL1_N, HIGH); if (outputDEBUG) {Serial.print("IPL210 ");}; break;
        case 0b110: digitalWriteFast(uP_IPL2_0_N, HIGH); digitalWriteFast(uP_IPL1_N, HIGH); if (outputDEBUG) {Serial.print("IPL21_ ");}; break;
        case 0b101: digitalWriteFast(uP_IPL2_0_N, HIGH);                                    if (outputDEBUG) {Serial.print("IPL2_0 ");}; break;
        case 0b100: digitalWriteFast(uP_IPL2_0_N, HIGH);                                    if (outputDEBUG) {Serial.print("IPL2__ ");}; break;
        case 0b011: digitalWriteFast(uP_IPL2_0_N, HIGH); digitalWriteFast(uP_IPL1_N, HIGH); if (outputDEBUG) {Serial.print("IPL_10 ");}; break;
        case 0b010:                                      digitalWriteFast(uP_IPL1_N, HIGH); if (outputDEBUG) {Serial.print("IPL_1_ ");}; break;
        case 0b001: digitalWriteFast(uP_IPL2_0_N, HIGH);                                    if (outputDEBUG) {Serial.print("IPL__0 ");}; break;
        case 0b000:                                                                         if (outputDEBUG) {Serial.print("IPL___ ");}; break;
      }

      // 68008  expects vector # in D7..D0.
      // - alternatively, VPA can be asserted for auto-vector.  
      //   Unfortunately we can't do auto-vector b/c we use DTACK
      //   for bus transactions. hence we will mimic.
      //   auto-vector number is ($18+interrupt level).

      // Mimic auto-vectored... Override if you like.
      switch(level)
      {
        case 0b111: vector = 0x18 + 0b111; break;
        case 0b110: vector = 0x18 + 0b110; break;
        case 0b101: vector = 0x18 + 0b101; break;
        case 0b100: vector = 0x18 + 0b100; break;
        case 0b011: vector = 0x18 + 0b011; break;
        case 0b010: vector = 0x18 + 0b010; break;
        case 0b001: vector = 0x18 + 0b001; break;
        case 0b000: vector = 0x18 + 0b000; break;
      }

      xDATA_DIR_OUT();
      SET_DATA_OUT(vector);
      BUFFER_DELAY();

      // for debug console
      uP_DATA = DATA_OUT;      
    }
    else
    //////////////////////////////
    // Data bus - WRITE
    //////////////////////////////
    if (digitalReadFast(uP_MEGA_RW_INV))
    {

      if (outputDEBUG) { Serial.print(" WR "); }

      DATA_IN = xDATA_IN();

      // RAM1
      if ( (RAM1_START <= uP_ADDR) && (uP_ADDR <= RAM1_END) )
      { 
        RAM1[uP_ADDR - RAM1_START] = DATA_IN; 
        if (outputDEBUG) { Serial.print("RAM1 "); }
      }
      else
      // RAM2
      if ( (RAM2_START <= uP_ADDR) && (uP_ADDR <= RAM2_END) )
      { 
        RAM2[uP_ADDR - RAM2_START] = DATA_IN; 
        if (outputDEBUG) { Serial.print("RAM2 "); }
      }
      else
      if (uP_ADDR == M6850_CTRL_STAT_1)
      {
        m6850_regCTRL_1 = DATA_IN;
        if (outputDEBUG) { Serial.print("6850_1 "); }
      }
      else
      if (uP_ADDR == M6850_TX_RX_1)
      {
        Serial.write( m6850_regTXD_1 = DATA_IN );
        // Blink LED
        if (LED_HDD_timeout == 0)
        {
          LED_HDD_timeout = millis();
          digitalWriteFast(LED_HDD, LOW);
        }        
        if (outputDEBUG) { Serial.print(" 6850_1 "); }
      }
      else
      if (uP_ADDR == M6850_CTRL_STAT_2)
      {
        m6850_regCTRL_2 = DATA_IN;
        if (outputDEBUG) { Serial.print("6850_2 "); }
      }
      else
      if (uP_ADDR == M6850_TX_RX_2)
      {
        // Serial.print("AUX: "); 
        Serial.write( m6850_regTXD_2 = DATA_IN );
        // Blink LED
        if (LED_HDD_timeout == 0)
        {
          LED_HDD_timeout = millis();
          digitalWriteFast(LED_HDD, LOW);
        }        
        if (outputDEBUG) { Serial.print("6850_2 "); }
      }
      else
      {
        if (1 || outputDEBUG) { Serial.print(" W-UNK 0x"); Serial.println(uP_ADDR, HEX); delay(10); }
      }
      // Debugging purposes
      uP_DATA = DATA_IN;
    }
    else
    //////////////////////////////
    // Data bus - READ
    //////////////////////////////
    {

      if (outputDEBUG) { Serial.print(" RD "); }

      xDATA_DIR_OUT();

      // ROM?
      if ( (ROM_START <= uP_ADDR) && (uP_ADDR <= ROM_END) )
      { 
        DATA_OUT = pgm_read_byte_near( ROM + (uP_ADDR - ROM_START) );   // ROM[(uP_ADDR - ROM_START)]; 
        if (outputDEBUG) { Serial.print("ROM "); }
      }
      else 
      // RAM1?
      if ( (RAM1_START <= uP_ADDR) && (uP_ADDR <= RAM1_END) )
      { 
        DATA_OUT = RAM1[uP_ADDR - RAM1_START]; 
        if (outputDEBUG) { Serial.print("RAM1 "); }
      }
      else
      // RAM2?
      if ( (RAM2_START <= uP_ADDR) && (uP_ADDR <= RAM2_END) )
      { 
        DATA_OUT = RAM2[uP_ADDR - RAM2_START]; 
        if (outputDEBUG) { Serial.print("RAM2 "); }
      }
      else
      if ( (uP_ADDR == M6850_CTRL_STAT_1))
      {
        DATA_OUT = m6850_regSTAT_1; 
        if (outputDEBUG) { Serial.print("6850_1 "); }
      }
      else
      if ( (uP_ADDR == M6850_TX_RX_1))
      {
        DATA_OUT = m6850_regRXD_1;
        m6850_regSTAT_1 = m6850_regSTAT_1 & (0b11111110);       // Clear Char Received? bit
        if (outputDEBUG) { Serial.print("6850_1 "); }
      }    
      else
      if ( (uP_ADDR == M6850_CTRL_STAT_2))
      {
        DATA_OUT = m6850_regSTAT_2;
        if (outputDEBUG) { Serial.print("6850_2 "); }
      }
      else
      if ( (uP_ADDR == M6850_TX_RX_2))
      {
        DATA_OUT = m6850_regRXD_2;
        m6850_regSTAT_2 = m6850_regSTAT_2 & (0b11111110);       // Clear Char Received? bit
        if (outputDEBUG) { Serial.print("6850_2 "); }
      }    
      else
      {
        DATA_OUT = 0x00; 
        if (1 || outputDEBUG) { Serial.print(" R-UNK 0x"); Serial.println(uP_ADDR, HEX); delay(10); }
      }

      SET_DATA_OUT(DATA_OUT);
      BUFFER_DELAY();

      // for debug console
      uP_DATA = DATA_OUT;
    }

    if (outputDEBUG)
    {
      sprintf(tmp, " DATA: \"%02X\"", uP_DATA);
      Serial.write(tmp);
    }

    // For edge detection.
    prev_RESET_N = digitalReadFast(uP_RESET_N);
    prev_HALT_N  = digitalReadFast(uP_HALT_N);

    //////////////////////////////
    // Pulse DTACK to complete bus cycle
    //////////////////////////////

    digitalWriteFast(uP_PULSE_DTACK, HIGH); 
      DELAY_UNIT_50NS(); 
    digitalWriteFast(uP_PULSE_DTACK, LOW);
    
    // 68008 will continue bus transaction.
    // until it hits pause on the next one.

    if (LED_HDD_timeout > 0)
    {
      if ((millis() - LED_HDD_timeout) > LED_TIMEOUT_MS)
      {
        // Turn  off LED  after x milliseconds.
        LED_HDD_timeout = 0;
        digitalWriteFast(LED_HDD, HIGH);
      } 
    }

    DELAY_UNIT_250NS(); // Wait BLINDLY for DS to go away

    xDATA_DIR_IN();     // Set data buffer back to input
                        // Timing of this can be longer because
                        // the FF will disable 245 when DS goes high.
                        // We just don't want to hi-z before DS goes high
                        // otherwise we'll violate data hold.

    if (outputDEBUG) { Serial.println("*"); }

  }  //DS

}

////////////////////////////////////////////////////////////////////
// Interrupt Generation
////////////////////////////////////////////////////////////////////

#define TIMER_INT_FREQ      (10) //Hz.
#define TIMER_INT_PERIOD_MS (1000/TIMER_INT_FREQ) 
unsigned long prevMillis    = 0;

void cpu_process_interrupts()
{
  //////////////////////////////////////////////////
  // IPL0_2
  //////////////////////////////////////////////////

  // Periodic Timer interrupt.  NOT USED FOR TSBUG2
  if (0 && (millis() - prevMillis) > TIMER_INT_PERIOD_MS)      // millis() overflows in 49ish days???
  {
    prevMillis = millis();
    digitalWriteFast(uP_IPL1_N,   HIGH);    // Deassert IPL1 if already.
    digitalWriteFast(uP_IPL2_0_N, LOW);
    // Serial.print(".");
  }

  //////////////////////////////////////////////////
  // IPL_1
  //////////////////////////////////////////////////

  // Interrupt using FT245 RBF and no IPL0/2 (timer)  NOT USED FOR TSBUG2
  if (0 && Serial.available() && digitalReadFast(uP_IPL2_0_N))
  {
    digitalWriteFast(uP_IPL1_N, LOW);
  }
}

////////////////////////////////////////////////////////////////////
// Serial Event
////////////////////////////////////////////////////////////////////

inline __attribute__((always_inline))
void serialEvent6850() 
{
  if (Serial.available())
    if ((m6850_regSTAT_1 & 0x01) == 0x00)      // read serial byte only if char already processed
    {
      int ch = toupper( Serial.read() );
      m6850_regRXD_1 = ch;               
      m6850_regSTAT_1 = m6850_regSTAT_1 | 0x01;       // set Char Received? bit
    }
  return;
}

////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////

void setup() 
{
  Serial.begin(115200);
  while (!Serial);

  Serial.write(27);       // ESC command
  Serial.print("[2J");    // clear screen command
  Serial.write(27);
  Serial.print("[H");
  Serial.println("\n");
  Serial.println("Configuration:");
  Serial.println("==============");
  print_teensy_version();
  Serial.print("Debug:      ");   Serial.println(outputDEBUG, HEX);
  Serial.print("--------------"); Serial.println();
  Serial.print("ROM Size:   ");   Serial.print(ROM_END - ROM_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("ROM_START:  0x"); Serial.println(ROM_START, HEX); 
  Serial.print("ROM_END:    0x"); Serial.println(ROM_END, HEX);
  Serial.print("--------------"); Serial.println(); 
  Serial.print("RAM1 Size:  ");   Serial.print(RAM1_END - RAM1_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("RAM1_START: 0x"); Serial.println(RAM1_START, HEX); 
  Serial.print("RAM1_END:   0x"); Serial.println(RAM1_END, HEX); 
  Serial.print("RAM2 Size:  ");   Serial.print(RAM2_END - RAM2_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("RAM2_START: 0x"); Serial.println(RAM2_START, HEX); 
  Serial.print("RAM2_END:   0x"); Serial.println(RAM2_END, HEX); 
  Serial.print("--------------"); Serial.println(); 
  Serial.println("======================================================================");
  Serial.println("MOTOROLA EDUCATION COMPUTER BOARD");
  Serial.println("TUTOR ROM REVISION # 1.3");
  Serial.println("                                                          Nov 12 1988");
  Serial.println("");
  Serial.println(" This archive contains the source code for the TUTORBUG monitor");
  Serial.println(" which is used on the M68000 Educational Board (ECB). A Users manual is");
  Serial.println(" available from Motorola Literature Distribution for approx. $5.00. The");
  Serial.println(" manual may be ordered as MEX68KECB/DD from the following address:");
  Serial.println("");
  Serial.println(" MOTOROLA SPS INC");
  Serial.println(" Literature Dist. Center");
  Serial.println(" PO Box 20924");
  Serial.println(" Phoenix, Az. 85036-0924");
  Serial.println("");
  Serial.println(" (602) 994-6561");
  Serial.println("");
  Serial.println(" Ron.");
  Serial.println("");
  Serial.println(" ...THANKS to by http://www.easy68k.com/paulrsm/mecb/mecb.htm");
  Serial.println("*=======================================================================*");
  Serial.println("Please read the manual (./docs/ folder) for monitor commands.");
  Serial.println("Example: MD 8000 20;DI     Disassemble memory for 0x20 bytes");
  Serial.println("Example: MM 3000;DI         Enter assembly code (Space required)");
  Serial.println("");
  Serial.println("         Remember TO hit SPACE after ? for entering assembly. ");
  Serial.println("         ...SPACE is required because no labels are allowed and the");
  Serial.println("         ... format matches the resident assembler.");

  // Since Retroshield 68008 does not cut into the MECB boards :) I thought it's ok to use
  // the rom image for educational purposes.  This will help people learn 68008 programming,
  // even though it may not be applicable to the new world order anymore :)
  //
   
  if (outputDEBUG)
  {
    Serial.println();
    Serial.println(". DEBUG Mode enabled.");
    Serial.println("  Press (P) to single-step.");
    Serial.println();
    delay(1000);
  }

  uP_init();          // processor specific init
  board_init();       // board specific init, like peripheral init, overriding rom, etc.
  
  // Reset processor
  Serial.print("\nRESET=1...");
  uP_reset_TWICE();   // Need 2nd reset to reset our two FF's; require DS 0->1 to reset.

  Serial.println("RESET=0...\n");
}

////////////////////////////////////////////////////////////////////
// Loop
////////////////////////////////////////////////////////////////////

void loop()
{
  byte i = 0;

  // Loop forever
  //  
  while(1)
  {
    cpu_tick(); 

    if (i++ == 0)
    {
      serialEvent6850();
      cpu_process_interrupts();
      Serial.flush();
    }  

    //////////////////////////////
    // Check for buttons (P) and (C)
    // WARNING these slow down execution
    //   quite a lot due to analogRead & 
    //   16ms debounce delays.
    //////////////////////////////

    //////////////////////////////
    // Check for reset button (C)
    if (0 && btn_C_debounced()) 
    {
      uP_assert_reset();
      delay(200);
      uP_release_reset();
      delay(200);      
      Serial.println("\n\n## RESET applied. ##\n");
    }  

    //////////////////////////////
    // Use (P) button to single-step.
    if (outputDEBUG)
    {
      while(!btn_P_debounced());
      while(btn_P_debounced());
    }

  }
}
