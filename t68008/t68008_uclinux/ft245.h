#ifndef _FT245_H
#define _FT245_H

////////////////////////////////////////////////////////////////////
// FT245 Implementation
//
////////////////////////////////////////////////////////////////////

#define SERIN           0x00078000
#define SEROUT          0x0007A000
#define SERSTATUS_RDF   0x0007C000
#define SERSTATUS_TXE   0x0007D000

// LED Display out
#define LED_DOUT        0x0007E000
#define LED_TIMEOUT_MS  50
unsigned long           LED_HDD_timeout;

#endif