#ifndef _MEMORY_MAP_H
#define _MEMORY_MAP_H

#include <Arduino.h>

////////////////////////////////////////////////////////////////////
// RAM
////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////
#if (ARDUINO_AVR_MEGA2560)
////////////////////////////////////////////////////////////////////

#define       RAM1_START   ((word) 0x0000)
#define       RAM1_END     ((word) 0x17FF)     // User RAM
byte          RAM1[RAM1_END-RAM1_START+1];

#define       RAM2_START   ((word) 0x1800)
#define       RAM2_END     ((word) 0x18FF)     
byte          RAM2[RAM2_END-RAM2_START+1];

////////////////////////////////////////////////////////////////////
#elif ((ARDUINO_TEENSY35 || ARDUINO_TEENSY36) || (ARDUINO_TEENSY41))
////////////////////////////////////////////////////////////////////

#define       RAM1_START   ((word) 0x0000)
#define       RAM1_END     ((word) 0xFFFF)    
byte          RAM1[RAM1_END-RAM1_START+1];

#endif

////////////////////////////////////////////////////////////////////
// ROMs
//
// Convert bin to hex at http://tomeko.net/online_tools/file_to_hex.php?lang=en
////////////////////////////////////////////////////////////////////

// ROMs
#define ROM1         EPROM1
#define ROM1_START   ((word) 0x3800)
#define ROM1_END     ((word) (ROM1_START+sizeof(ROM1)-1) )

// #define ROM2         EPROM2
// #define ROM2_START   ((word) 0x3000)
// #define ROM2_END     ((word) (ROM2_START+sizeof(ROM2)-1) )


////////////////////////////////////////////////////////////////////
// Monitor Code
////////////////////////////////////////////////////////////////////
#define STORE_ROM_IN_FLASH  1  // 0: Save ROM in RAM, 1: Save ROM in FLASH

#ifdef ROM1
#if STORE_ROM_IN_FLASH
const unsigned char EPROM1[] PROGMEM = {
#else
unsigned char EPROM1[] = {
#endif
#include "eeprom1.h"     // org $3800: Intellec 8/Mod 8 Monitor
};
#endif

#ifdef ROM2
#if STORE_ROM_IN_FLASH
const unsigned char EPROM2[] PROGMEM = {
#else
unsigned char EPROM2[] = {
#endif
#include "eeprom2.h"     // org $3800: Intellec 8/Mod 8 Monitor
};
#endif

////////////////////////////////////////////////////////////////////
// Used by RetroShell to access ROM
// These functions read/write ROM/RAM automatically.
////////////////////////////////////////////////////////////////////

inline __attribute__((always_inline))
byte memory_read_byte(word addr, byte unknown_location_value)
{
  ////////// ROM1 //////////
#ifdef ROM1
  if ((addr >= ROM1_START) && (addr <= ROM1_END )) {
  #if STORE_ROM_IN_FLASH
    return pgm_read_byte_near(ROM1 + (addr - ROM1_START));
  #else
    return ROM1[addr - ROM1_START];
  #endif
  } else
#endif

  ////////// ROM2 //////////
#ifdef ROM2
  if ((addr >= ROM2_START) && (addr <= ROM2_END )) {
  #if STORE_ROM_IN_FLASH
    return pgm_read_byte_near(ROM2 + (addr - ROM2_START));
  #else
    return ROM2[addr - ROM2_START];
  #endif  
  } else
#endif

  ////////// RAM1 //////////
#ifdef RAM1_START
  if ((addr >= RAM1_START) && (addr <= RAM1_END))
    return RAM1[addr - RAM1_START];
  else
#endif

  ////////// RAM2 //////////
#ifdef RAM2_START
  if ((addr >= RAM2_START) && (addr <= RAM2_END))
    return RAM2[addr - RAM2_START];
  else
#endif

  // unknown location
  return unknown_location_value;
}

inline __attribute__((always_inline))
bool memory_write_byte(word addr, byte value)
{
  ////////// ROM1 //////////
#ifdef ROM1
  if ((addr >= ROM1_START) && (addr <= ROM1_END)) 
  { 
  #if STORE_ROM_IN_FLASH
    Serial.println("ERR: Can not write ROM1 in FLASH.");
    return false;
  #else
    ROM1[addr - ROM1_START] = value;
    return true;
  #endif
  } else
#endif

  ////////// ROM2 //////////
#ifdef ROM2
  if ((addr >= ROM2_START) && (addr <= ROM2_END)) 
  { 
  #if STORE_ROM_IN_FLASH
    Serial.println("ERR: Can not update ROM2 in FLASH.");
    return false;
  #else
    ROM2[addr - ROM2_START] = value;
    return true;
  #endif
  }
  else
#endif

  ////////// RAM1 //////////
#ifdef RAM1_START
  if ((addr >= RAM1_START) && (addr <= RAM1_END))
  {
    RAM1[addr - RAM1_START] = value;
    return true;
  }
  else
#endif

  ////////// RAM2 //////////
#ifdef RAM2_START
  if ((addr >= RAM2_START) && (addr <= RAM2_END))
  {
    RAM1[addr - RAM2_START] = value;
    return true;
  }
  else
#endif

  return false;
}

#endif // _MEMORYMAP_H

