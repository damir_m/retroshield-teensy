#ifndef _SETUPHOLD_H
#define _SETUPHOLD_H

// ##################################################
// Adjust setup/hold times based on board
// Arduino Mega 2560  = 16Mhz   (0x)
// Teensy 3.5         = 120Mhz  (1x)
// Teensy 3.6         = 180Mhz  (1.5x)
// Teensy 4.1         = 600Mhz  (5x)
// ##################################################


// ##################################################
#if (ARDUINO_AVR_MEGA2560)
// ##################################################
  #define DELAY_UNIT()          asm volatile("nop\nnop\nnop\n")
  #define DELAY_FACTOR_H()      {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT();} // DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }
  #define DELAY_FACTOR_L()      {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT();} // DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }

  #define DELAY_FOR_BUFFER()    asm volatile("nop")   // N/A for Mega (Delay for level shifters (teensy) to pass data out)

// ##################################################
#elif (ARDUINO_TEENSY35)
// ##################################################

  #define DELAY_UNIT()      asm volatile("nop\nnop\nnop\nnop\nnop\nnop\n")
  #define DELAY_FACTOR_H() {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }
  #define DELAY_FACTOR_L() {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }

  // Add about (8.5+5.1ns = 13.6 ~ 20ns) delay for TXB0108's to stabilize.
  // 1/120MHz * 4 = 32ns  
  #define DELAY_FOR_BUFFER()  asm volatile("nop\nnop\nnop\nnop\n");

// ##################################################
#elif (ARDUINO_TEENSY36)
// ##################################################

  #define DELAY_UNIT()      asm volatile("nop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\n")
  #define DELAY_FACTOR_H() {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }
  #define DELAY_FACTOR_L() {DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }

  // Add about (8.5+5.1ns = 13.6 ~ 20ns) delay for TXB0108's to stabilize.
  // 1/180MHz * 5 = 16ns
  #define DELAY_FOR_BUFFER()  asm volatile("nop\nnop\nnop\nnop\nnop\n");

// ##################################################
#elif (ARDUINO_TEENSY41)
// ##################################################

  #define DELAY_UNIT_25NS() asm volatile("nop\nnop\nnop\nnop\nnop\nnop\n" \
                                         "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                         "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                         "nop\nnop\nnop\n" \
                                         "nop\nnop\nnop\nnop\nnop\nnop\n")

  #define DELAY_UNIT()      asm volatile("nop\nnop\nnop\nnop\nnop\nnop\n" \
                                         "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                         "nop\nnop\nnop\nnop\nnop\nnop\n" \
                                         "nop\nnop\nnop\n" \
                                         "nop\nnop\nnop\nnop\nnop\nnop\n")
  #define DELAY_FACTOR_1HL() delayNanoseconds(250) //{DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }
  #define DELAY_FACTOR_1LL() delayNanoseconds(150) //{DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }
  #define DELAY_FACTOR_2LH() delayNanoseconds(150) //{DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }
  #define DELAY_FACTOR_2LL() delayNanoseconds(150) //{DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); DELAY_UNIT(); }

  // delayMicroseconds(1) // 
  // Add about (8.5+5.1ns = 13.6 ~ 20ns) delay for TXB0108's to stabilize.
  // 1/600MHz * 10 = 16ns
  #define DELAY_FOR_BUFFER()  asm volatile("nop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\nnop\n");
  
#endif


// ##################################################
// NEXT_EDGE CLOCK Functions.
// - dummy loop until it's time for next clock edge.
// ##################################################
// CREDITS
//
// These functions are based on the following
//
// 1) https://github.com/ryomuk/Teensy-4001/
//    Copyright (c) 2023 Ryo Mukai
// 2) https://forum.pjrc.com/index.php?threads/teensy-4-intervaltimer-max-speed.57959/#post-218577
//    Teensy 4 IntervalTimer Max Speed
// 3) https://github-wiki-see.page/m/TeensyUser/doc/wiki/Using-the-cycle-counter
//    Using the cycle counter - TeensyUser/doc GitHub Wiki


// ##################################################
#if (ARDUINO_AVR_MEGA2560)
#error "Sorry - Arduino Mega is not supported w/ this code. Please download Retroshield-Arduino repo."
// ##################################################
#elif (ARDUINO_TEENSY36)
#error "Sorry - Teensy 3.6 is not supported. Please use Teensy4.1."
// ##################################################
#elif ((ARDUINO_TEENSY35) || (ARDUINO_TEENSY36) || (ARDUINO_TEENSY41))
// ##################################################

  #define TEENSY_PERIOD_NS          (1e9/F_CPU)                   // ns duration
  #define NS_TO_TEENSY_CYCLE(N)     ((N)/TEENSY_PERIOD_NS)        // Convert ns to #cpu cycle

  uint32_t uP_CLOCK_FREQ;           // set by set_clock_frequency_kHz()
  uint32_t NEXT_EDGE_CLK1_W;        // t_PW = 380-480ns   (diff=100ns)
  uint32_t NEXT_EDGE_D1;            // t_D1 = 400-550ns   (diff=150ns)
  uint32_t NEXT_EDGE_CLK2_W;        // t_PW = 380-480ns   (diff=100ns)
  uint32_t NEXT_EDGE_D2;            // t_D2 = 150-300ns or 2000ns - (CLK1_W + D1 + CLK2_W)
  uint32_t next_edge_offset = 13;   // Fine-tune knob for freq accuracy.   set_clock_frequency_kHz()

// ##################################################
  inline __attribute__((always_inline))
  uint32_t get_clock_frequency_Hz()
  {                         
    return uP_CLOCK_FREQ; 
  }

  inline __attribute__((always_inline))
  void set_clock_frequency_Hz(uint32_t freq)
  {                         
    uint32_t clk_period_tn = NS_TO_TEENSY_CYCLE(1e9/freq);   // clock period in terms of Teensy CLK

    uP_CLOCK_FREQ = freq; 
    // Calculate delays between PHI1 and PHI2
    NEXT_EDGE_CLK1_W   = NS_TO_TEENSY_CYCLE(250);      // >60ns
    NEXT_EDGE_D1       = NS_TO_TEENSY_CYCLE(250);       // >0ns 
    NEXT_EDGE_CLK2_W   = NS_TO_TEENSY_CYCLE(250);     // >220ns
    NEXT_EDGE_D2       =                              // >70ns
                         (clk_period_tn) - (NEXT_EDGE_CLK1_W + NEXT_EDGE_D1 + NEXT_EDGE_CLK2_W);
    
    // Feb73 & Mar87 datasheet don't specify t_D2 max (inferred from max clock period 2000ns).
    // Nov71 datasheet shows t_D2 max 300ns. We distribute the extra over 300ns.
    // if (1 && (NEXT_EDGE_D2 > NS_TO_TEENSY_CYCLE(300)))
    // {
    //   uint32_t extra    = NEXT_EDGE_D2 - NS_TO_TEENSY_CYCLE(300);
    //   // if (extra > ((300) /TEENSY_PERIOD_NS))
    //   //   extra = ((300) /TEENSY_PERIOD_NS);
    //   NEXT_EDGE_CLK1_W  = NEXT_EDGE_CLK1_W  + extra/3;
    //   NEXT_EDGE_D1      = NEXT_EDGE_D1      + extra/3;
    //   NEXT_EDGE_CLK2_W  = NEXT_EDGE_CLK2_W  + extra/3;
    //   NEXT_EDGE_D2      = NS_TO_TEENSY_CYCLE(300);
    // }

    #if (ARDUINO_TEENSY35)
      next_edge_offset = 13;    // DO NOT CHANGE; important for freq calibration.
    #elif (ARDUINO_TEENSY36)
      next_edge_offset = 13;    // DO NOT CHANGE; important for freq calibration.
    #elif (ARDUINO_TEENSY41)
      if (freq > 729999)
        next_edge_offset = 15;  // DO NOT CHANGE; important for freq calibration.
      else
        next_edge_offset = 13;  // DO NOT CHANGE; important for freq calibration.
    #endif

    if (1 || outputDEBUG)
    {
      Serial.write("\n");    
      Serial.write("Teensy Frq = ");  Serial.print(F_CPU/1000000);            Serial.write(" MHz.\n");
      Serial.write("uP Freq    = ");  Serial.print(uP_CLOCK_FREQ/1000);       Serial.write(" kHz.\n");
      Serial.write("CLK1_W     = ");  Serial.print(NEXT_EDGE_CLK1_W);         Serial.write(" cyc.\n");
      Serial.write("Dly 1-2    = ");  Serial.print(NEXT_EDGE_D1);             Serial.write(" cyc.\n");
      Serial.write("CLK2_W     = ");  Serial.print(NEXT_EDGE_CLK2_W);         Serial.write(" cyc.\n");
      Serial.write("Dly 2-1    = ");  Serial.print(NEXT_EDGE_D2);             Serial.write(" cyc.\n");
      Serial.write("Offset     = ");  Serial.print(next_edge_offset);         Serial.write(" cyc.\n");
      Serial.write("\n");    
    }

    // for ARM DWT
    // req'd for 3.5/3.6
    ARM_DEMCR     |= ARM_DEMCR_TRCENA;
    ARM_DWT_CTRL  |= ARM_DWT_CTRL_CYCCNTENA;
  }

// ##################################################
  inline __attribute__((always_inline))
  void next_edge_setup_ns(uint32_t clk1_w, uint32_t d1, uint32_t clk2_w, uint32_t d2, uint32_t offset)
  {
    // Manual mode, delays (ns) set by User.
    NEXT_EDGE_CLK1_W    = NS_TO_TEENSY_CYCLE(clk1_w);     // MAX = 480ns
    NEXT_EDGE_D1        = NS_TO_TEENSY_CYCLE(d1);         // MAX = 550ns
    NEXT_EDGE_CLK2_W    = NS_TO_TEENSY_CYCLE(clk2_w);     // MAX = 480ns    
    NEXT_EDGE_D2        = NS_TO_TEENSY_CYCLE(d2);         // MAX = 300ns
    next_edge_offset    = NS_TO_TEENSY_CYCLE(offset);

    // for ARM DWT
    // req'd for 3.5/3.6
    ARM_DEMCR     |= ARM_DEMCR_TRCENA;
    ARM_DWT_CTRL  |= ARM_DWT_CTRL_CYCCNTENA;
  }

// ##################################################
  inline __attribute__((always_inline))
  void next_edge_start()
  {
    ARM_DWT_CYCCNT = 0; /* reset counter */
  }

// ##################################################
  inline __attribute__((always_inline))
  void wait_for_edge_abs(uint32_t next_time)
  {
    while(ARM_DWT_CYCCNT < next_time);
    ARM_DWT_CYCCNT = next_edge_offset;   /* reset counter with compensation */
    return;
  }
#endif  // 
// ##################################################

#endif  // _SETUPHOLD_H