#ifndef INTELLEC_H
#define INTELLEC_H

#include <Arduino.h>

////////////////////////////////////////////////////////////////////
// Bit definitions
////////////////////////////////////////////////////////////////////
#define TTC_RBIT    0x01       // TTY READER GO/NO GO   
#define TTC_PCMD    0x02       // PTP GO/NO GO
#define TTC_RCMD    0x04       // PTR GO/NO GO
#define TTC_DSB     0x08       // PROM ENABLE/DISABLE
#define TTC_DIN_TC  0x10       // PTP GO/NO GO
#define TTC_DOUT_TC 0x20       // PTR GO/NO GO
#define TTC_PBIT    0x40       // 1702 PROM PROG GO/NO GO
#define TTC_PBITA   0x80       // 1702A PROM PROG GO/NO GO

#define TTS_TTYDA    0x01       // DEF: 1 / 0: TTY INPUT IS READY
#define TTS_TTYOE    0x02       // DEF: 1 / 0: TTY Overrun Error
#define TTS_TTYBE    0x04       // DEF: 0: TTY Buffer Empty
#define TTS_TTYFE    0x08       // DEF: 1 / 0: TTY Frame Error
#define TTS_TTYPE    0x10       // DEF: 1 / 0: TTY Parity Error
#define TTS_PTRDA    0x20       // DEF: 0 / 1: PTR HAS CHAR
#define TTS_PRDY     0x40       // DEF: 1 / 0: PTP IS READY
#define TTS_BIT7     0x80       // DEF: 1 Unassigned

#define CRT_DA     0x01       // DEF: 1  CRT 0: INPUT is READY
#define CRT_OE     0x02       // DEF: 1  CRT Overrun Error
#define CRT_BE     0x04       // DEF: 0  CRT Buffer Empty
#define CRT_FE     0x08       // DEF: 1  CRT Frame Error
#define CRT_PE     0x10       // DEF: 1  CRT Parity Error

////////////////////////////////////////////////////////////////////
// I/O Addresses
////////////////////////////////////////////////////////////////////
#define ADDR_TTY_IN     0x00       // TTY input port (INVERTED)
#define ADDR_TTY_OUT    0x08       // TTY output port (INVERTED)
#define ADDR_TTS        0x01       // Device status input port (INVERTED)
#define ADDR_TTC        0x09       // Device control output port (INVERTED)

#define ADDR_CRT_IN     0x04       // CRT control output port (INVERTED)
#define ADDR_CRT_STATUS 0x05       // CRT control output port (INVERTED)
#define ADDR_CRT_OUT    0x04       // CRT control output port (INVERTED)

#define ADDR_PTR_IN     0x03       // PTR input port (NOT INVERTED)
#define ADDR_PTP_OUT    0x0B       // PTP output port (NOT INVERTED)

////////////////////////////////////////////////////////////////////
// Registers
////////////////////////////////////////////////////////////////////
byte reg_TTY_IN  = 0x00;
byte reg_TTY_OUT = 0x00;
byte reg_CRT_IN  = 0x00;
byte reg_PTR_IN  = 0x00;
byte reg_PTP_OUT = 0x00;

byte reg_TTC  = (TTC_RBIT || TTC_DSB);
byte reg_TTS  = (0b11011011);        // TTY no receive, TTY output is ready, PTRDA has no char, PTP is ready
byte reg_CRTS = (0b11111011);       // CRT control output port

////////////////////////////////////////////////////////////////////
// AY-5-1012/1013 USART Peripheral
// emulate just enough so keyboard/display works thru serial port.
////////////////////////////////////////////////////////////////////
//

void intellec_init()
{
  reg_TTC  = (0b00001000);
  reg_TTS  = (0b01011011);        // TTY no receive, TTY output is ready, PTRDA has no char, PTP is ready
  reg_CRTS = (0b00011011);       // CRT control output port
}

////////////////////////////////////////////////////////////////////
// Serial Event
////////////////////////////////////////////////////////////////////

inline __attribute__((always_inline))
void serialEventIntellec(bool use_interrupt) 
{
  const byte  TRANSMIT_DELAY  = 30;               // tx_delay between chars
  static byte tx_delay        = TRANSMIT_DELAY;
  
  if (tx_delay > 0)
    tx_delay--;
  else
  if (use_interrupt && (digitalReadFast(PIN_TO_ASSERT_INT) == LOW))       // TODO: Edit per the processor's INT pin.
  {
    // If interrupt is already asserted, wait for it go high...
    return;
  }
  else
  if (Serial.available())
  {
    reg_TTS = reg_TTS & ((byte) ~TTS_TTYDA);

    if (use_interrupt) digitalWriteFast(PIN_TO_ASSERT_INT, LOW);       // TODO: Edit per the processor's INT pin.  

    tx_delay = TRANSMIT_DELAY;
  }
  return;
}


int intellec_read_io(word ADDR_L)
{
  switch(ADDR_L)
  {
    case ADDR_TTY_IN:
                          // digitalWriteFast(MEGA_INT, HIGH);       // TODO: Change the interrupt pin to the processor's INT pin.
                          reg_TTS = reg_TTS | (TTS_TTYDA);
                          DBG_LED_On(LED_DEBUG);
                          return ~(toupper(Serial.read()));
    case ADDR_TTS:
                          return reg_TTS;
    case ADDR_CRT_IN:
                          DBG_LED_On(LED_DEBUG);
                          return ~(toupper(Serial.read()));
    case ADDR_CRT_STATUS:
                          DBG_LED_On(LED_DEBUG);
                          return reg_CRTS;
    case ADDR_PTR_IN:
                          DBG_LED_On(LED_DEBUG);
                          return ~(toupper(Serial.read()));

    default:
        return -1;
  }
}


// Write data to 8251 if ADDR_L is in the range.
// Return false if not in range.
//
bool intellec_write_io(word ADDR_L, byte DATA_IN)
{
  switch(ADDR_L)
  {
    case ADDR_TTY_OUT:    // TTY output port (INVERTED)
      // Serial.print("."); 
      Serial.write((byte) ~DATA_IN);
      DBG_LED_On(LED_DEBUG);
      return true;
      break;
    case ADDR_TTC:        // Device control output port (INVERTED)
      // Serial.print("\n*"); 
      // Serial.println((byte) ~DATA_IN, HEX);
      reg_TTC = ~DATA_IN;
      return true;
      break;

    case ADDR_CRT_OUT:    // CRT control output port (INVERTED)
      // Serial.print(","); 
      Serial.write((byte) ~DATA_IN);
      DBG_LED_On(LED_DEBUG);
      return true;
      break;
    case ADDR_PTP_OUT:    // PTP output port (NOT INVERTED)
      // Serial.print("!"); 
      Serial.write((byte) ~DATA_IN);
      DBG_LED_On(LED_DEBUG);
      return true;
      break;

    default:
      return false;
  }
}


#endif