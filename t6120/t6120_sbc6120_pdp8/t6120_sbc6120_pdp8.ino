////////////////////////////////////////////////////////////////////
// RetroShield 6120 for Teensy
// 2024/01/10
// Version 0.3
// Erturk Kocalar
//
// The MIT License (MIT)
//
// Copyright (c) 2019 Erturk Kocalar, 8Bitforce.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// Date         Comments                                            Author
// -----------------------------------------------------------------------------
// 08/08/2021   Initial Bring-up.                                   Erturk
// 02/23/2023   Rewrite for cleaner version                         Erturk
// 01/10/2024   Add Teensy 4.1 support                              Erturk
// 02/25/2024   Clean-up and release.                               Erturk
// 02/29/2024   Clean-up in loop() - thanks to Mark Jungwirth       Erturk
// 04/04/2024   Added new OS8 image; optimized for a bit more speed Erturk
//
////////////////////////////////////////////////////////////////////
// Options
//   These are for debugging purposes. May not work if you enable.
////////////////////////////////////////////////////////////////////

#define outputDEBUG 0
#define outputINFO  0
#define outputERROR 1
#define SLOW_MODE   100     // used to delay clock cycles during debugging
char    tmp[220];           // used for debug prints

word clock_cycle_count;
bool SD_Card_Present = false;

////////////////////////////////////////////////////////////////////
// Libraries
////////////////////////////////////////////////////////////////////
#include <SPI.h>
#include <SD.h>

#include "memorymap.h"      // Memory Map (ROM, RAM, PERIPHERALS)
#include "setuphold.h"      // Delays required to meet setup/hold
#include "portmap.h"        // Pin mapping to cpu
#include "sbc6120_ide.h"    // IDE emulation enough for os8
#include "dsk_iot.h"        // Work in progress - DF32 emulation from David/Ian.

// ##################################################
// CPU Init
// ##################################################

void uP_init()
{
  // Set directions for ADDR & DATA Bus.
  // !!!!  Initial use of pinMode required for Teensy b/c it sets up pinmux and direction.
  configure_PINMODE_ADDR();
  configure_PINMODE_DATA();

  pinMode(uP_READ_N,    INPUT);
  pinMode(uP_WRITE_N,   INPUT);
  pinMode(uP_SKIP_N,    OUTPUT);
  pinMode(uP_MEMSEL_N,  INPUT);
  pinMode(uP_IOCLR_N,   INPUT);
  pinMode(uP_LXDAR_N,   INPUT);
  pinMode(uP_LXPAR_N,   INPUT);
  pinMode(uP_LXMAR_N,   INPUT);
  pinMode(uP_RESET_N,   OUTPUT);
  pinMode(uP_DATAF_N,   INPUT);
  pinMode(uP_INTGNT_N,  INPUT);

  pinMode(uP_INTREQ_N,  OUTPUT);
  pinMode(uP_CPREQ_N,   OUTPUT);
  pinMode(uP_EMA2,      INPUT);
  pinMode(uP_C1N_EMA1,  INPUT);
  pinMode(uP_C0N_EMA0,  INPUT);
  pinMode(uP_CLK,       OUTPUT);

  pinMode(FORCE_C0,     OUTPUT);
  pinMode(FORCE_C1,     OUTPUT);
  pinMode(FORCE_SKIP,   OUTPUT);

  uP_assert_reset();

  clock_cycle_count = 0;
}

void uP_assert_reset()
{

  digitalWriteFast(uP_SKIP_N,   LOW);
  digitalWriteFast(uP_INTREQ_N, HIGH);
  digitalWriteFast(uP_CPREQ_N,  HIGH);

  disable_FORCE();

  // Drive RESET conditions
  digitalWriteFast(uP_RESET_N, LOW);
}

void uP_release_reset()
{
  // Drive RESET conditions
  digitalWriteFast(uP_RESET_N, HIGH);
}



////////////////////////////////////////////////////////////////////
// Processor Control Variables
////////////////////////////////////////////////////////////////////

bool prev_uP_LXMAR_N  = true;
bool prev_uP_LXDAR_N  = true;
bool prev_uP_LXPAR_N  = true;
bool prev_uP_MEMSEL_N = true;
bool prev_uP_READ_N   = true;
bool prev_uP_WRITE_N  = true;
bool prev_uP_IOCLR_N  = true;
bool prev_uP_DATAF_N  = true;
bool prev_uP_INTGNT_N = true;

bool prev_uP_SKIP_N   = true;
bool prev_uP_C0N_EMA0 = false;    // MSB
bool prev_uP_C1N_EMA1 = false;    // MSB-1
bool prev_uP_EMA2     = false;    // MSB-2
word prev_DX          = 0;
word prev_ADDR        = 0;
word ADDR_LATCHED     = 0;
word DATA_LATCHED     = 0;
bool DATAF_LATCHED    = 0;

byte POST_CODE  = 0x00;

byte trace_hdr_cnt = 0;

// CREDIT: IIRC below is text from SBC6120 PAL source code.
//         I'm basically replicating what the PALs do.
//         text copy/pasted is from the PAL source code.
  
/*   The SBC6120 has three memory subsystems - an 8K by 12 bit ROM made       */
/* up of two 27C64 EPROMS in parallel (the extra four bits are unused),       */
/* a 64K by 12 bit read/write memory made up of three 6208 SRAMs, and         */
/* 2Mb by 8 bit battery backed up RAM array (up to four 628512LP SRAMs)       */
/* used as a nonvolatile RAM disk.  Our problem is to fit all those in        */
/* to the two address spaces, panel memory and main memory, supported         */
/* by the HM6120.                                                             */
/*                                                                            */
/*   We do it with the aid of two memory mapping inputs which define          */
/* four different memory maps:                                                */
/*                                                                            */
/*    Direct Addressing Indirect Addressing                                   */
/*    ----------------- -------------------                                   */
/*  Map 0      EPROM           RAM                                            */
/*  Map 1       RAM            EPROM                                          */
/*  Map 2       RAM            RAM                                            */
/*  Map 3       RAM            DISK                                           */
/*                                                                            */
/*   IMPORTANT - these memory maps apply only to panel memory.  All */
/* main memory accesses always go to the the 6208 RAMs regardless of  */
/* the mapping mode!              */
// $define MAP0 (!MEM_MAP_0 & !MEM_MAP_1)  /* selected by IOT 6400 */
// $define MAP1 ( MEM_MAP_0 & !MEM_MAP_1)  /*    "     "   "  6401 */
// $define MAP2 (!MEM_MAP_0 &  MEM_MAP_1)  /*    "     "   "  6402 */
// $define MAP3 ( MEM_MAP_0 &  MEM_MAP_1)  /*    "     "   "  6403 */

bool MAP0     = true;       // selected by IOT 6400
bool MAP1     = false;      // "        "  IOT 6401
bool MAP2     = false;      // "        "  IOT 6402
bool MAP3     = false;      // "        "  IOT 6403

bool ROM_CE   = false;
bool RAM_CS   = false;
bool RD_SR    = false;
bool WR_SR    = false;
bool DISK_CE  = false;
bool DASP     = false;      // Disk drive activity, from IDE connector
bool BYTE_READ= false;

byte DAR_REG  = 0x00;       // RAMDisk register

bool LOAD_PRINTER_BUF   = false;      // arduino can read the byte from 6120
char TX_BUF             = 0x00;
char RX_BUF             = 0x00;
bool PRINTER_FLAG       = true;       // ready to print
bool KEYBOARD_FLAG      = false;      // ready to receive
bool KEYBOARD_BUF_READ  = true;       // arduino read the byte

bool POST_FLG   = true;     // post code updated; set to true by default to print it on POR


bool IOT_IN_IOCLEAR = false;
bool first_time = true;
word ADDR_LATCHED_ROM = 0;
word ADDR_LATCHED_RAM = 0;

bool prev_ROM_READ   = false;
bool      ROM_READ   = false;
bool prev_RAM_READ   = false;
bool      RAM_READ   = false;
bool prev_RAM_WRITE  = false;
bool      RAM_WRITE  = false;


void k6120_trace_signals()
{
  if (!outputDEBUG)
    return;

  if (trace_hdr_cnt == 0)
  {
    sprintf(tmp, "\n          ST R# | CK MA PA DA  | MS RD WR OUT| IF IOC| IR IG CPR| DF E2 E1 E0 SK |  DX\n");
    Serial.write(tmp);
  }

  trace_hdr_cnt++;
  if (trace_hdr_cnt == 10)
    trace_hdr_cnt =0;


  // sprintf(tmp, "%.1X  %.4X  %.1X  %.1X  %.1X  %.1X  | %.1X  %.1X  %.1X  | %.1X  %.1X  %.1X  | %.1X  %.1X  %.1X  | %.1X  %.1X  | %.1X  %.1X  %.1X  %.1X  |",
  sprintf(tmp, "%.8X | %c %c  | %s  %c  %c  %c  | %c  %c  %c  %c  | %c  %c  | %c  %c  %c  | %c  %c  %c  %c  %c  | ",
    clock_cycle_count,

    '.', // digitalRead(uP_STRTUP) ? '#' : '.',
    digitalRead(uP_RESET_N) ? '.' : 'R',

    digitalRead(uP_CLK) ? "0." : ".1",
    digitalRead(uP_LXMAR_N) ? '.' : 'M',
    digitalRead(uP_LXPAR_N) ? '.' : 'P',
    digitalRead(uP_LXDAR_N) ? '.' : 'D',

    digitalRead(uP_MEMSEL_N) ? '.' : 'M',
    digitalRead(uP_READ_N) ? '.' : 'R',
    digitalRead(uP_WRITE_N) ? '.' : 'W',
    '.', // digitalRead(uP_OUT_N) ? '.' : 'O',

    '.', // digitalRead(uP_IFETCH_N) ? '.' : 'I',
    digitalRead(uP_IOCLR_N)   ? '.' : 'C',

    digitalRead(uP_INTREQ_N)  ? '.' : '#',
    digitalRead(uP_INTGNT_N)  ? '.' : '#',
    digitalRead(uP_CPREQ_N)   ? '.' : '#',

    digitalRead(uP_DATAF_N)   ? '.' : 'D',
    digitalRead(uP_EMA2)      ? '1' : '0',
    digitalRead(uP_C1N_EMA1)  ? '1' : '0',
    digitalRead(uP_C0N_EMA0)  ? '1' : '0',
    digitalRead(uP_SKIP_N)    ? '.' : 'S'
  );

  Serial.write(tmp);
  sprintf(tmp, " %.4o (%.3X)", xDATA_IN(), xDATA_IN() );
  Serial.write(tmp);
  sprintf(tmp, " : ADDR: %.4o, ROM/RAM/SR_RD/SR_WR: %c%c%c%c", ADDR_LATCHED, ROM_CE ? 'O' : '.', RAM_CS ? 'A' : '.', RD_SR ? 'R' : '.', WR_SR ? 'W' : '.');
  Serial.write(tmp);
  sprintf(tmp, " DISK/READ_BYTE: %.1X/%.1X\n", DISK_CE, BYTE_READ);
  Serial.write(tmp);
}



////////////////////////////////////////////////////////////////////
// Processor Control Loop
//
// TODO: write up how hd-6120 changes signals for bus transactions
//       i.e. some signals change when CLK=high, and others change
//            when CLK=low. We focus on CLK=low and do edge detection
//            to handle bus transactions. I definitely lost some hair
//            decrypting the datasheet.
//
////////////////////////////////////////////////////////////////////

// byte rom_read = 3;
bool readmodify_access = false;

bool POST_0_ONCE = true;
bool POST_1_ONCE = true;

inline __attribute__((always_inline))
void cpu_tick()
{

  // ##################################################
  // CLOCK = LOW
  // ##################################################
  CLK_LOW();
  DELAY_FACTOR_L(); 

  // ##################################################
  // Capture processor control signals.
  // Note polarity change for most frequent use
  // ##################################################
  bool LXPAR = !digitalReadFast(uP_LXPAR_N);
  bool LXMAR = !digitalReadFast(uP_LXMAR_N);
  bool LXDAR = !digitalReadFast(uP_LXDAR_N);
  //bool DATAF = !digitalReadFast(uP_DATAF_N);
  bool READ  = !digitalReadFast(uP_READ_N);
  bool WRITE = !digitalReadFast(uP_WRITE_N);

  // ##################################################
  // Latch addresses on falling edge of LXMAR, LXPAR, LXDAR.
  // ##################################################
  if (  (prev_uP_LXMAR_N && LXMAR) ||
        (prev_uP_LXPAR_N && LXPAR) ||
        (prev_uP_LXDAR_N && LXDAR)    )
  {
    ADDR_LATCHED  =  prev_ADDR;
    DATAF_LATCHED = !prev_uP_DATAF_N;
    // ^^^ using prev_XXX because by the time teensy reads the pins,
    //     we may be out of the hold time.

    if (outputDEBUG || outputINFO)
    {
      // Serial.print("     *************** = \n");
      // Serial.print("     ADDRESS LATCHED = "); Serial.print(ADDR_LATCHED, OCT); Serial.print(" - "); Serial.println(ADDR_LATCHED & 07777, OCT);
      sprintf(tmp, "     ADDRESS LATCHED = (%.1o) %.4o => ", ADDR_LATCHED >> 12 & 07, ADDR_LATCHED & 07777);
      Serial.write(tmp);

      readmodify_access = false;     // Reset to print read/modify nicely.
    }
  }

  if (outputDEBUG)
  {
    k6120_trace_signals();
  }

  // CREDIT: Below is from SBC6120 PAL source code.
  //         I'm basically replicating what the PALs do.

  /*    Direct Addressing     Indirect Addressing   IOT         */
  /*    -----------------     -------------------   --------    */
  /*  Map 0      EPROM                RAM           IOT 6400    */
  /*  Map 1       RAM                EPROM          IOT 6401    */
  /*  Map 2       RAM                 RAM           IOT 6402    */
  /*  Map 3       RAM                 DISK          IOT 6403    */

  /*   The EPROM is enabled in mode 0 for direct reads from panel memory  */
  /* and in mode 1 for indirect reads from panel memory.  EPROM is only   */
  /* enabled for panel memory accesses and never for main memory, and it  */
  /* is never enabled for a write operation in any mapping mode.  The     */
  /* 27C64 output enable can be driven directly by the 6120 READ signal,  */
  /* and we have no need to worry about that here.      */
  // ROM_CE = LXPAR & READ & ((MAP0 & !DATAF) # (MAP1 & DATAF));

  ROM_CE = LXPAR && READ && ( ((MAP0 && !DATAF_LATCHED) || (MAP1 && DATAF_LATCHED)) );

  /*   The 6208 RAMs are enabled for any read or write access to main     */
  /* memory, regardless of the mapping mode.  They are also enabled for   */
  /* any write access to panel memory _except_ indirect writes in map 3,  */
  /* which write to the RAM disk instead.  Finally, the RAMs are enabled  */
  /* for indirect reads from panel memory in mode 0, direct reads in mode */
  /* 1 or 3, and any read in mode 2.  Whew!                               */
  /*                                                                      */
  /*   Note that the 6208s don't have any output enable pin, and their    */
  /* output drivers are on any time their chip select is asserted, so to  */
  /* prevent bus contention we have to gate its chip select with READ.    */
  //  RAM_CS = ((READ # WRITE) & LXMAR)
  //    # (WRITE & LXPAR & !(MAP3 & DATAF))
  //    # (READ & LXPAR &
  //              ((MAP0 &  DATAF) # (MAP1 & !DATAF) # MAP2 # (MAP3 & !DATAF)));

  RAM_CS = ((READ || WRITE) &&  LXMAR) ||
           (WRITE && LXPAR && !(MAP3 &&  DATAF_LATCHED)) ||
           (READ  && LXPAR && ((MAP0 &&  DATAF_LATCHED)  ||
                               (MAP1 && !DATAF_LATCHED)  ||
                               (MAP2                  )  ||
                               (MAP3 && !DATAF_LATCHED)));

  /*   The last two signals, RD_SR and WR_SR, are asserted by the OSR and */
  /* WSR instructions, respectively.  These are easy, since the 6120      */
  /* addresses the switch register by a READ or WRITE in the absence of   */
  /* any LXMAR, LXPAR or LXDAR...                                         */

  RD_SR = READ  && !(LXMAR || LXPAR || LXDAR);
  WR_SR = WRITE && !(LXMAR || LXPAR || LXDAR);

  /*   The RAM disk is enabled only for indirect access to panel memory   */
  /* when map 3 is in use and since the 628512 SRAMs used in the RAM disk */
  /* have both output enable and write enable pins, we don't have to      */
  /* worry about either READ or WRITE in our logic here.  The BYTE READ L */
  /* signal is asserted for all read from RAM disk operations - this      */
  /* enables an external 74HC365 tristate buffer that drives DX[0:3] low  */
  /* so that all RAM disk reads will always reliably read zeros for the   */
  /*  upper four bits.                                                    */
  DISK_CE   =          LXPAR && MAP3 && DATAF_LATCHED;
  BYTE_READ = READ  && LXPAR && MAP3 && DATAF_LATCHED;


  // EDGE DETECTORS
  //

  ROM_READ   = (ROM_CE && READ);
  RAM_READ   = (RAM_CS && READ);
  RAM_WRITE  = (RAM_CS && WRITE);

  // We will use edges to respond to read & write bus activity.
  // we output data to processor on read falling edge.
  // we act on data from processor when write goes high.
  //
  bool ROM_READ_FALLING_EDGE  = (!prev_ROM_READ && ROM_READ);
  bool RAM_READ_FALLING_EDGE  = (!prev_RAM_READ && RAM_READ);
  bool RAM_WRITE_RISING_EDGE  = (prev_RAM_WRITE && !RAM_WRITE);

  // ##################################################
  // ROM Operation
  // ##################################################
  if (ROM_READ_FALLING_EDGE) // (ROM_CE && READ_FALLING_EDGE) // READ)
  {
    // MAP0
    ADDR_LATCHED_ROM = ADDR_LATCHED & 037777; // limit to 8K during map1

    DATA_LATCHED = (  (ROM_H[ ADDR_LATCHED_ROM ] & 0x3F) << 6)
                    | (ROM_L[ ADDR_LATCHED_ROM ] & 0x3F);


    xDATA_DIR_OUT();
    SET_DATA_OUT(DATA_LATCHED);

    if (outputDEBUG || outputINFO)
    {
      //const char *mpanel  = "PANEL";
      //const char *mmain   = "MAIN";
      const char *mm0p    = "MAP0";
      const char *mm1p    = "MAP1";
      const char *mm2p    = "MAP2";
      const char *mm3p    = "MAP3";
      const char *memory_string = "     ";
      char *map_string;

      if (MAP0) map_string = (char *)mm0p;
      if (MAP1) map_string = (char *)mm1p;
      if (MAP2) map_string = (char *)mm2p;
      if (MAP3) map_string = (char *)mm3p;

      sprintf(tmp, "%s - %s ROM read  (%.6o) = %.4o\n", map_string, memory_string, ADDR_LATCHED_ROM, DATA_LATCHED);
      Serial.write(tmp);
    }
  }
  else
  // ##################################################
  // RAM Operation
  // ##################################################

  if (RAM_READ_FALLING_EDGE) // (RAM_CS && READ_FALLING_EDGE) // READ)
  {
    ADDR_LATCHED_RAM = ADDR_LATCHED;
    if (!LXMAR) { // digitalReadFast(uP_LXMAR_N)) {
      // A15=0 for Main RAM (32K)
      // A15=1 for Panel Memory (32K)
      ADDR_LATCHED_RAM = ADDR_LATCHED_RAM | 0x8000;
    }

    DATA_LATCHED = ((RAM_H[ ADDR_LATCHED_RAM ] & 0x3F) << 6) |
                   ((RAM_L[ ADDR_LATCHED_RAM ] & 0x3F)     );

    xDATA_DIR_OUT();
    SET_DATA_OUT(DATA_LATCHED);

    if (outputDEBUG || outputINFO)
    {
      const char *mpanel  = "PANEL";
      const char *mmain   = "MAIN";
      const char *mm0p    = "MAP0";
      const char *mm1p    = "MAP1";
      const char *mm2p    = "MAP2";
      const char *mm3p    = "MAP3";
      char *memory_string;
      char *map_string;

      if (digitalReadFast(uP_LXMAR_N))
        memory_string = (char *)mpanel;
      else
        memory_string = (char *)mmain;

      if (MAP0) map_string = (char *)mm0p;
      if (MAP1) map_string = (char *)mm1p;
      if (MAP2) map_string = (char *)mm2p;
      if (MAP3) map_string = (char *)mm3p;

      sprintf(tmp, "%s - %s RAM read  (%.6o) = %.4o\n", map_string, memory_string, ADDR_LATCHED_RAM & 0x7FFF, DATA_LATCHED);
      Serial.write(tmp);

      readmodify_access = true;  // in case processor does read/modify
    }
  }
  else
  if (RAM_WRITE_RISING_EDGE) // (RAM_CS && WRITE_RISING_EDGE) // WRITE)
  {
    ADDR_LATCHED_RAM = ADDR_LATCHED;
    if (!LXMAR) { // digitalReadFast(uP_LXMAR_N)) {
      // A15=0 for Main RAM
      // A15=1 for Panel Memory
      ADDR_LATCHED_RAM = ADDR_LATCHED_RAM | 0x8000;
    }

    DATA_LATCHED = prev_DX; // xDATA_IN();
    RAM_H[ (ADDR_LATCHED_RAM - RAM_START) ] = (DATA_LATCHED >> 6) & 0x3F;
    RAM_L[ (ADDR_LATCHED_RAM - RAM_START) ] = (DATA_LATCHED     ) & 0x3F;

    if (outputDEBUG || outputINFO)
    {
      const char *mpanel  = "PANEL";
      const char *mmain   = "MAIN ";
      const char *mm0p    = "MAP0";
      const char *mm1p    = "MAP1";
      const char *mm2p    = "MAP2";
      const char *mm3p    = "MAP3";
      const char *memory_string;
      const char *map_string;

      if (digitalReadFast(uP_LXMAR_N))
        memory_string = mpanel;
      else
        memory_string = mmain;

      if (MAP0) map_string = mm0p;
      if (MAP1) map_string = mm1p;
      if (MAP2) map_string = mm2p;
      if (MAP3) map_string = mm3p;

      if (readmodify_access)
      {
        Serial.print("                                   ");
      }
      sprintf(tmp, "%s - %s RAM write (%.6o) = %.4o\n", map_string, memory_string, ADDR_LATCHED_RAM & 0x7FFF, DATA_LATCHED);
      Serial.write(tmp);
    }
  }
  else
  // IOT SECTION
  //
  if (LXDAR || !digitalReadFast(uP_IOCLR_N))     // Treat stand-alone IOCLR as IOT, i.e. CAF...
  {

    // We will support these IOT's:
    // To add more, add the #define here and then add the
    // case below.
    //

#define POST      06440   // POST LED (lest significant 3 bits)

#define MM0       06400   // MM0 : Selecet ROM/RAM memory map
#define MM1       06401   // MM0 : Selecet RAM/ROM memory map
#define MM2       06402   // MM0 : Selecet RAM only memory map
#define MM3       06403   // MM0 : Selecet RAM/RAM disk memory map

#define KCF       06030   // KCF : Clear keyboard flag
#define KSF       06031   // KSF : Skip if console receive flag is set
#define KCC       06032   // KCC : Clear console receive flag and AC
#define KRS       06034   // KRS : OR AC with console receive buffer
#define KRB       06036   // KRB : Read receive buffer into AC and clear the flag

#define TFL       06040   // TFL : Set transmit flag, but not the AC
#define TSF       06041   // TSF : Skip if console transmit flag is set
#define TCF       06042   // TCF : Clear transmit flag, but not the AC
#define TPL       06044   // TPC : Load AC into transmit buffer, but don't clear flag
#define TLS       06046   // TLS : Load AC into transmit buffer and clear flag

#define PPI_RD_0  06470   // PRPA : Read PPI port A   (AC_CLEAR + OR_READ)
#define PPI_RD_1  06471   // PRPB : Read PPI port B   (AC_CLEAR + OR_READ)
#define PPI_RD_2  06472   // PRPC : Read PPI port C   (AC_CLEAR + OR_READ)
#define PPI_RD_3  06473   // PRCR : Read PPI Control Register   (AC_CLEAR + OR_READ)

#define PPI_WR_4  06474   // PWPA : Write PPI port A and clear the AC
#define PPI_WR_5  06475   // PWPB : Write PPI port B and clear the AC
#define PPI_WR_6  06476   // PWPC : Write PPI port C and clear the AC

#define PPI_WR_7  06477   // PWCR : Write PPI Control Register and clear the AC

#define LDAR      06410   // LDAR : Load RAM disk address register and clear AC
#define SDRQ      06411   // SDASP : Skip on IDE activity

#define PRISLU    06412   // Select SLU console mode (device codes 03/04)
#define SECSLU    06413   // Select SLU secondary mode (device codes 36/37)
#define SBBLO     06415   // Skip on RAM disk backup battery low

#define CCPR      06430   // Clear control panel request flags
#define SHSW      06431   // Skip on HALT SWITCH request
#define SPLK      06432   // Skip on PANEL LOCK
#define SCPT      06433   // Skip on Control Panel Timer request
#define RFNS      06434   // Read function switches
#define RLOF      06435   // RUN LED off
#define RLON      06437   // RUN LED on

#define OSR       07404   // OR with switch register
#define WSR       06246   // Write data LEDs (rotary switch != MD)

#define DCMA      06601   // Clear Disk Memory Address Register
#define DMAR      06603   // Load Disk Memory Address Register and Read
#define DMAW      06605   // Load Disk Memory Address Register and Write
#define DCEA      06611   // Clear Disk Extended Address Register
#define DSAC      06612   // Skip on Address Confirmed Flag
#define DEAL      06615   // Load Disk Extended Address
#define DEAC      06616   // Read Disk Extended Address
#define DFSE      06621   // Skip on Zero Error Flag
#define DFSC      06622   // Skip on Data Completion Flag
#define DMAC      06626   // Read Disk Memory Address Register
#define DISK_WC   07750   // Three-cycle data break (DMA) Word Count
#define DISK_CA   07751   // Three-cycle data break (DMA) Current Address

    // ##################################################
    // Reset devices upon reset
    // ##################################################
    if ( !digitalReadFast(uP_IOCLR_N) || !digitalReadFast(uP_RESET_N) || IOT_IN_IOCLEAR)
    {
      // Reset IOT devices once, when
      // 1) power-on, when RESET + IOCLR are asserted.
      // 2) CAF instruction which only asserts IOCLR.

      if (first_time)
      {
        Serial.println("     RESET IOT DEVICES");
        // Reset Display IOT
        // Reset Keyboard IOT
        // Reset expanded memory IOT
        // Reset harddisk IOT

        DAR_REG           = 0x00;     // Disk access register
        POST_CODE         = 0x00;
        POST_FLG          = false;

        TX_BUF            = 0;
        PRINTER_FLAG      = true;      // 1: char from 6120 has been printed. Ready for next char.
        RX_BUF            = 0;
        KEYBOARD_FLAG     = false;      // 1: 6120 needs to read a char from KEYBOARD_BUF. Generate interrupt when set.
        KEYBOARD_BUF_READ = true;       // arduio printed RX_BUF -- combine w/ KEYBOARD_FLAG.

        PPI_04_PPA  = 0xFF;
        PPI_15_PPB  = 0xFF;
        PPI_26_PPC  = 0xFF;
        PPI_37_CC   = 0x9B;

        dsk_clear();                  // df32 emulation.
      }

      IOT_IN_IOCLEAR = true;          // stay in this section until IOCLR goes high.
      first_time = false;             // IOCLR is multiple cycles long.
    }
    else
    {

      // IOT instr placed on DX0..DX11
      // datafield on C0, C1, EMA2
      // DATAF asserted
      //    LXDAR => LOW
      //    WRITE signal
      //        C0/C1 define type of instruction 
      //                C0  C1
      //                H   H  - (AC) => (Device)
      //                L   H  - (AC) => (Device), AC Clear-ed
      //                H   L  - (Device OR AC) => (AC)
      //                L   L  - (Device) => (AC)
      //        SKIP => skip next instruction

      // Level shifters on teensy board does not have enough drive
      // to drive C0/C1/SKIP to gnd during IOT. Hence we use
      // open-drain gates to drive them as needed. These are
      // open drain so during non-IOT, HD6120 can drive them
      // towards Teensy.
      //
      #define IOT_AC_CLEAR()        drive_C0_N(LOW)
      #define IOT_READ_AC()         drive_C1_N(LOW)
      #define IOT_SKIP()            drive_SKIP_N(LOW)

      word IOT_LATCHED            = ADDR_LATCHED & 07777;
      bool IOT_WRITE_FALLING_EDGE = ( prev_uP_WRITE_N &&  WRITE);
      bool IOT_WRITE_RISING_EDGE  = (!prev_uP_WRITE_N && !WRITE);
      //bool IOT_WRITE_LOW_LEVEL    = WRITE;
      //bool IOT_READ_LOW_LEVEL     = READ;
      bool IOT_READ_FALLING_EDGE  = (  prev_uP_READ_N &&  READ);
      bool IOT_READ_RISING_EDGE   = ( !prev_uP_READ_N && !READ);

      // Handle IOT functions based on READ/WRITE rising or falling edges.
      // To add new IOT's, add case below.
      //

      // TEMPLATE TO ADD YOUR OWN IOT...
      //
      // Copy/paste this case section and fill in the appropriate sections.
      //
          // case DEAC: // #define DEAC      06616   // Read Disk Extended Address
          //   if (IOT_WRITE_FALLING_EDGE) { IOT_SKIP(); IOT_AC_CLEAR(); IOT_READ_AC(); }
          //                             //  ^^ Pick feedback to 6120 as appropriate
          //                             //  IOT_SKIP() - tell 6120 to skip next instruction
          //                             //  IOT_AC_CLEAR() - AC will be cleared before OR'ing.
          //                             //  IOT_READ_AC() - IOT has Read cycle following this write.
          //   else
          //   if (IOT_WRITE_RISING_EDGE)   // Process WRITES upon rising edge, to ensure 
          //   {                            // we are using the latest data from 6120.
          //     dskrg = prev_DX;
          //     if (outputDEBUG || outputINFO) { Serial.print("... IOT DEAL : Load Disk Extended Address 0x"); Serial.println(dskrg, HEX); Serial.flush();};
          //   }
          //   if (IOT_READ_FALLING_EDGE)
          //   {
          //     // Read started, start outputing data to 6120
          //     xDATA_DIR_OUT();
          //     SET_DATA_OUT(dskrg);
          //     if (outputDEBUG || outputINFO) { Serial.print("... IOT DEAC : Read Disk Extended Address 0x"); Serial.println(dskrg, HEX); Serial.flush(); };
          //   }
          //   else
          //   if (IOT_READ_RISING_EDGE)
          //   {
          //     // When read cycle is complete...
          //     // If some flags needs to be cleared on read, clear those
          //     // flags on read rising edge, i.e. when the read cycle is complete.
          //     //
          //   }
          //   break;
          //   // Don't forget the break!

      switch(IOT_LATCHED)
      {
        /**************************************************************************************************************/
        case RLOF:      // 06435   // RUN LED 
            if (IOT_WRITE_RISING_EDGE)
            {
              if (outputDEBUG || outputINFO) { Serial.println(" ## RUN LED  = OFF ## "); }
            }
            break;
        case RLON:      // 06437   // RUN LED on
            if (IOT_WRITE_RISING_EDGE) 
            {
              if (outputDEBUG || outputINFO) { Serial.println(" ## RUN LED  = ON ## "); }
            }
            break;
        /**************************************************************************************************************/
        case PPI_RD_0:  // 06470   // PRPA : Read PPI port A   (AC_CLEAR + OR_READ)
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); IOT_READ_AC(); }
            else
            if (IOT_READ_FALLING_EDGE)
            {
              if (outputDEBUG || outputINFO) { Serial.print("... IOT PPI_RD_PPA : 0x"); Serial.println(PPI_04_PPA, HEX); };
              xDATA_DIR_OUT();
              SET_DATA_OUT(PPI_04_PPA);
            }
            break;
        case PPI_RD_1:  // 06471   // PRPB : Read PPI port B   (AC_CLEAR + OR_READ)
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); IOT_READ_AC(); }
            else
            if (IOT_READ_FALLING_EDGE)
            {
              if (outputDEBUG || outputINFO) { Serial.print("... IOT PPI_RD_PPB : 0x"); Serial.println(PPI_15_PPB, HEX); };
              xDATA_DIR_OUT();
              SET_DATA_OUT(PPI_15_PPB);
            }
            break;
        case PPI_RD_2:  // 06472   // PRPC : Read PPI port C   (AC_CLEAR + OR_READ)
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); IOT_READ_AC(); }
            else
            if (IOT_READ_FALLING_EDGE)
            {
              if (outputDEBUG || outputINFO) { Serial.print("... IOT PPI_RD_PPC : 0x"); Serial.println(PPI_26_PPC, HEX); };
              xDATA_DIR_OUT();
              SET_DATA_OUT(PPI_26_PPC);
            }
            break;
        case PPI_RD_3:  // 06473   // PRPC : Read PPI Control Register   (AC_CLEAR + OR_READ)
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); IOT_READ_AC(); }
            else
            if (IOT_READ_FALLING_EDGE)
            {
              if (outputDEBUG || outputINFO) { Serial.print("... IOT PPI_RD_CC  : 0x"); Serial.println(PPI_37_CC, HEX); };
              xDATA_DIR_OUT();
              SET_DATA_OUT(PPI_37_CC);
            }
            break;
        /**************************************************************************************************************/
        case PPI_WR_4:
            // PWPA : Write PPI port A and clear the AC
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); }
            else
            if (IOT_WRITE_RISING_EDGE)
            {
              PPI_04_PPA = prev_DX & 0xFF;
              if (outputDEBUG || outputINFO) // || outputERROR)
              {
                sprintf(tmp, "... IOT PPI_WR_PPA : 0x%.2X\n", PPI_04_PPA);
                Serial.write(tmp);
                // Serial.print("... IOT PPI_WR_PPA : "); Serial.print(PPI_04_PPA, HEX); Serial.println("");
              }
            }
            break;
        case PPI_WR_5:
            // PWPB : Write PPI port B and clear the AC
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); }
            else
            if (IOT_WRITE_RISING_EDGE)
            {
              PPI_15_PPB = prev_DX & 0xFF;
              if (outputDEBUG || outputINFO) // || outputERROR)
              {
                // Serial.print("... IOT PPI_WR_PPB : "); Serial.print(PPI_15_PPB, HEX); Serial.println("");
                sprintf(tmp, "... IOT PPI_WR_PPB : 0x%.2X\n", PPI_15_PPB);
                Serial.write(tmp);
              }
            }
            break;
        case PPI_WR_6:
            // PWPC : Write PPI port C and clear the AC
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); }
            else
            if (IOT_WRITE_RISING_EDGE)
            {
              PPI_26_PPC = prev_DX & 0xFF;
              if (outputDEBUG || outputINFO) // || outputERROR)
              {
                sprintf(tmp, "... IOT PPI_WR_PPC : 0x%.2X\n", PPI_26_PPC);
                Serial.write(tmp);
                // Serial.print("... IOT PPI_WR_PPC : "); Serial.print(PPI_26_PPC, HEX); Serial.println("");
              }

              // Check if IDE needs to respond
              if (ide_sdcard_img_present) check_for_ide_event();
            }
            break;
        case PPI_WR_7:
            // PWCR : Write PPI Control Register and clear the AC
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); }
            else
            if (IOT_WRITE_RISING_EDGE)
            {
              if (prev_DX & 0x80)               // bit7 active mode
                PPI_37_CC = prev_DX & 0xFF;
              else
              {
                // 8255 special write to set/reset bits in PPC (bit7=0)
                if (prev_DX & 0x01)
                  bitSet(PPI_26_PPC, (prev_DX >> 1) & 0x07);
                else
                  bitClear(PPI_26_PPC, (prev_DX >> 1) & 0x07);
              }
              if (outputDEBUG || outputINFO) // || outputERROR)
              {
                sprintf(tmp, "... IOT PPI_WR_CC  : 0x%.2X [PPC = 0x%.2X]\n", prev_DX, PPI_26_PPC);
                Serial.write(tmp);
                // Serial.print("... IOT PPI_WR_CC : 0x"); Serial.print(prev_DX, HEX); Serial.print(" [ PPC = "); Serial.println(PPI_26_PPC, HEX);
              }

              // Check if IDE needs to respond
              if (ide_sdcard_img_present) check_for_ide_event();
            }

            break;
        /**************************************************************************************************************/
        case POST+0:
        case POST+1:
            if (IOT_WRITE_RISING_EDGE)
            {
              POST_CODE = IOT_LATCHED & 07;
            // Dont' print because POST+0 and POST+1 seem to be disk activity lights for OS8 boot.
            // Execute only once upon reset for completeness.
              if ((POST_0_ONCE || POST_1_ONCE) & (outputDEBUG || outputINFO || outputERROR))
              {
                Serial.print("### POST CODE : ");
                switch(POST_CODE)
                {
                  case 1: Serial.println("1 ### Running Panel Monitor");  POST_1_ONCE = false;   break;
                  case 0: Serial.println("0 ### Running Main Memory");            POST_0_ONCE = false;   break;
                  default:
                    Serial.println("POST ERR ###");
                }
              }
            }
            break;
        case POST+2:
        case POST+3:
        case POST+4:
        case POST+5:
        case POST+6:
        case POST+7:
            if (IOT_WRITE_RISING_EDGE)
            {
              POST_CODE = IOT_LATCHED & 07;
              if (outputDEBUG || outputINFO || outputERROR)
              {
                Serial.print("### POST CODE : ");
                switch(POST_CODE)
                {
                  case 7: Serial.println("7 ### CPU");          break;
                  case 6: Serial.println("6 ### Panel RAM");    break;
                  case 5: Serial.println("5 ### ROM Checksum"); break;
                  case 4: Serial.println("4 ### Memory");       break;
                  case 3: Serial.println("3 ### Ext ROM");      break;
                  case 2: Serial.println("2 ### Console");      break;
                  default:
                    Serial.println("POST ERR ###");
                }
              }
            }
            break;
        /**************************************************************************************************************/
        case MM0:
            if (IOT_WRITE_RISING_EDGE) { MAP0 = true;    MAP1 = false;   MAP2 = false;   MAP3 = false; if (outputDEBUG || outputINFO) Serial.println("... MM0");}
            break;
        case MM1:
            if (IOT_WRITE_RISING_EDGE) { MAP0 = false;   MAP1 = true;    MAP2 = false;   MAP3 = false; if (outputDEBUG || outputINFO) Serial.println("... MM1");}
            break;
        case MM2:
            if (IOT_WRITE_RISING_EDGE) { MAP0 = false;   MAP1 = false;   MAP2 = true;    MAP3 = false; if (outputDEBUG || outputINFO) Serial.println("... MM2");}
            break;
        case MM3:
            if (IOT_WRITE_RISING_EDGE) { MAP0 = false;   MAP1 = false;   MAP2 = false;   MAP3 = true; if (outputDEBUG || outputINFO)  Serial.println("... MM3");}
            break;
        /**************************************************************************************************************/
        case LDAR:      // 06410   // LDAR : Load RAM disk address register and clear AC
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); }
            else
            if (IOT_WRITE_RISING_EDGE)
            {
              DAR_REG = ADDR_LATCHED & 0xF;
              if (outputDEBUG || outputINFO) { Serial.print("... LOAD DAR = "); Serial.println(DAR_REG, HEX); }
            }
            break;
        /**************************************************************************************************************/
        case PRISLU:    //06412   // Select SLU console mode (device codes 03/04)
            if (IOT_WRITE_FALLING_EDGE)
            {
              if (outputDEBUG || outputINFO || outputERROR) Serial.println("... PRISLU (IO @ 03/04)");
            }
            break;
        case TFL:       // 06040   // TFL : Set transmit flag, but not the AC
            if (IOT_WRITE_RISING_EDGE)
            {
              PRINTER_FLAG=true;
            }
            break;
        case TSF:       // 06041   // TSF : Skip if console transmit flag is set
            if (IOT_WRITE_FALLING_EDGE)
            {
              if (PRINTER_FLAG) IOT_SKIP();
            }
            break;
        case TCF:       // 06042   // TCF : Clear transmit flag, but not the AC
            if (IOT_WRITE_RISING_EDGE)
            {
              PRINTER_FLAG=false;
              if (outputDEBUG || outputINFO) { Serial.println("... IOT TCF"); Serial.flush(); };
            }
            break;
        case TPL:       // 06044   // TPC : Load AC into transmit buffer, but don't clear flag
            if (IOT_WRITE_RISING_EDGE)
            {
              TX_BUF = prev_DX & 0x7F;
              if (TX_BUF != 0) Serial.write(TX_BUF);
              PRINTER_FLAG = true;
              if (outputDEBUG || outputINFO) { Serial.print("... IOT TPL : 0x"); Serial.println(TX_BUF, HEX); Serial.flush(); };
            }
            break;
        case TLS:       // 06046   // TLS : Load AC into transmit buffer and clear flag
            if (IOT_WRITE_RISING_EDGE)
            {
              TX_BUF = prev_DX & 0x7F;
              if (TX_BUF != 0) Serial.write(TX_BUF);
              PRINTER_FLAG = true;
              if (outputDEBUG || outputINFO) { Serial.print("... IOT TLS : 0x"); Serial.println(TX_BUF, HEX); Serial.flush();};
            }
            break;
        /**************************************************************************************************************/
        case KCF:       // 06030   // KCF : Clear keyboard flag
            if (IOT_WRITE_RISING_EDGE)
            {
              KEYBOARD_FLAG     = false;
              KEYBOARD_BUF_READ = true;
              if (outputDEBUG || outputINFO) { Serial.println("... IOT KCF : clear flag"); Serial.flush(); };
            }
            break;
        case KSF:       // 06031   // KSF : Skip if console receive flag is set
            if (IOT_WRITE_FALLING_EDGE) { if (KEYBOARD_FLAG) IOT_SKIP(); }
            break;
        case KCC:       // 06032   // KCC : Clear console receive flag and AC
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); }
            else
            if (IOT_WRITE_RISING_EDGE)
            {
              KEYBOARD_FLAG     = false;
              KEYBOARD_BUF_READ = true;
              if (outputDEBUG || outputINFO) { Serial.println("... IOT KCC : clear flag and AC"); Serial.flush(); };
            }
            break;
        case KRS:       // 06034   // KRS : OR AC with console receive buffer
            if (IOT_WRITE_FALLING_EDGE) { IOT_READ_AC(); }
            else
            if (IOT_READ_FALLING_EDGE)
            {
              xDATA_DIR_OUT();
              SET_DATA_OUT(RX_BUF);
              // This IOT does not change flags. Just a peek.
              // KEYBOARD_FLAG     = false;
              // KEYBOARD_BUF_READ = true; 
              if (outputDEBUG || outputINFO) { Serial.print("... IOT KRS : 0x"); Serial.println(RX_BUF, HEX); Serial.flush(); };
            }
            break;
        case KRB:       // 06036   // KRB : Read receive buffer into AC and clear the flag
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); IOT_READ_AC(); }
            else
            if (IOT_READ_FALLING_EDGE)
            {
              xDATA_DIR_OUT();
              SET_DATA_OUT(RX_BUF);
              if (outputDEBUG || outputINFO) { Serial.print("... IOT KRB : 0x"); Serial.println(RX_BUF, HEX); Serial.flush(); };
            }
            else
            if (IOT_READ_RISING_EDGE)
            {
              KEYBOARD_FLAG     = false;
              KEYBOARD_BUF_READ = true; 
            }
            break;
        /**************************************************************************************************************/
          case DCMA: // #define DCMA      06601   // Clear Disk Memory Address Register
            if (IOT_WRITE_RISING_EDGE)
            {
              dskad = dskfl = 0;
              if (outputDEBUG || outputINFO) { Serial.println("... IOT DCMA : Clear Disk Memory Address Register"); Serial.flush(); };
            }
            break;
          case DMAR: // #define DMAR      06603   // Load Disk Memory Address Register and Read
          case DMAW: // #define DMAW      06605   // Load Disk Memory Address Register and Write
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); }
            else
            if (IOT_WRITE_RISING_EDGE)
            {
              // #define DISK_WC   07750   // Three-cycle data break (DMA) Word Count
              // #define DISK_CA   07751   // Three-cycle data break (DMA) Current Address

              break;    // 2024-0403: skip for now, work in progress.

              // Code from David & Ian's emulation.  See dsk_iot.h for details and copyright notice.
              unsigned int iii = (dskrg & 070) << 9;
              dskmem = ((read_memory(07751) + 1) & 07777) | iii; /* mem */
              tm = (dskrg & 03700) << 6;
              dskad = ( prev_DX & 07777) + tm; /* dsk */

              iii = 010000 - read_memory(07750);    // EK: number of words to copy over.
              // p = (uint8_t *)&mem[dskmem];       // EK: pointer to memory - we don't need it.
                                                    // EK: dskmem is the address we want to write to.
              dsk_seek(dskad);
              if (IOT_LATCHED == DMAR)
              {
                  dsk_read(dskmem, iii);
                  //Serial.printf("Read:%o>%o:%o,%d Len:%d\r\n", dskad, dskmem, dskrg, i, tmp);
              }
              else
              if (IOT_LATCHED == DMAW) 
              {
                  dsk_write(dskmem, iii);
                  //Serial.printf("Write:%o>%o:%o,%d Len:%d\r\n", dskad, dskmem, dskrg, i, tmp);
              }
              dskfl = 1;
              write_memory(07751, 0);       // CA: to-do. should not be 0.
              write_memory(07750, 0);       // WC:
              // acc = acc & 010000;            // EK: alreayd taken care of...


              if (outputDEBUG || outputINFO) { Serial.print("... IOT DEAL : Load Disk Extended Address 0x"); Serial.println(dskrg, HEX); Serial.flush();};
            }

            // IOT_LATCHED
            break;
          case DCEA: // #define DCEA      06611   // Clear Disk Extended Address Register
            if (IOT_WRITE_RISING_EDGE)
            {
              dskrg = 0;
              if (outputDEBUG || outputINFO) { Serial.println("... IOT DCEA : Clear Disk Extended Address Register"); Serial.flush(); };
            }
            break;
          case DSAC: // #define DSAC      06612   // Skip on Address Confirmed Flag
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); }
            break;
          case DEAL: // #define DEAL      06615   // Load Disk Extended Address
            // Clear disk ext addr and mem addr ext regs, load w/ track addr from AC
            // then OR registers + photocell mark + 3 error flags back into AC.
            // So we have write and read phases for this IOT.
            if (IOT_WRITE_FALLING_EDGE) { IOT_READ_AC(); }
            else
            if (IOT_WRITE_RISING_EDGE)
            {
              dskrg = prev_DX & 07777;
              if (outputDEBUG || outputINFO) { Serial.print("... IOT DEAL : Load Disk Extended Address 0x"); Serial.println(dskrg, HEX); Serial.flush();};
            }
            else
            if (IOT_READ_FALLING_EDGE)
            {
              xDATA_DIR_OUT();
              SET_DATA_OUT(dskrg);
              // if (outputDEBUG || outputINFO) { Serial.print("... IOT DEAL : Load Disk Extended Address 0x"); Serial.println(dskrg, HEX); Serial.flush(); };
            }
            // else
            // if (IOT_READ_RISING_EDGE)
            // {
            //   // When read cycle is complete...
            // }
            break;
          case DEAC: // #define DEAC      06616   // Read Disk Extended Address
            // IGNORE: "Skips the next instruction if address confirmed flag is one."
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); IOT_READ_AC(); }
            else
            if (IOT_READ_FALLING_EDGE)
            {
              xDATA_DIR_OUT();
              SET_DATA_OUT(dskrg);
              if (outputDEBUG || outputINFO) { Serial.print("... IOT DEAC : Read Disk Extended Address 0x"); Serial.println(dskrg, HEX); Serial.flush(); };
            }
            break;
          case DFSE: // #define DFSE      06621   // Skip on Zero Error Flag
            if (IOT_WRITE_FALLING_EDGE) { IOT_SKIP(); } 
            break;
          case DFSC: // #define DFSC      06622   // Skip on Data Completion Flag
            if (IOT_WRITE_FALLING_EDGE)
            {
              if (dskfl) IOT_SKIP();
            }
            break;
          case DMAC: // #define DMAC      06626   // Read Disk Memory Address Register
            if (IOT_WRITE_FALLING_EDGE) { IOT_AC_CLEAR(); IOT_READ_AC(); }
            else
            if (IOT_READ_FALLING_EDGE)
            {
              xDATA_DIR_OUT();
              SET_DATA_OUT(dskad & 07777);
              if (outputDEBUG || outputINFO) { Serial.print("... IOT DMAC : Read Disk Memory Address Register 0x"); Serial.println(dskad, HEX); Serial.flush(); };
            }
            else
            if (IOT_READ_RISING_EDGE)
            {
              // When read cycle is complete...
            }
            break;

        /**************************************************************************************************************/
        case CCPR:      // 06430   // Clear control panel request flags
            if (IOT_WRITE_FALLING_EDGE)
            {
              if (outputDEBUG || outputINFO) Serial.println("... CCPR (Clear control panel request flags)");
            }
            break;


  //  RFNS=6434               / read function switches (BOOT, EXAM, DEP, ROTSW, etc)
  //  /------------------------------------------------------------------------------+
  //  /      Bit definitions for the private Read Function Switches (RFNS) IOT       |
  //  /   0       1      2  |   3       4      5  |  6      7     8 |  9   10    11  |
  //  /+------+------+------+-------+------+------+-----+------+----+----+----+------+
  //  /| BOOT | LADR | LEXT | CLEAR | CONT | EXAM | DEP | HALT | MD | MQ | AC | STAT |
  //  /+------+------+------+-------+------+------+-----+------+----+----+----+------+
  //  Credit: https://www.grc.com/pdp-8/lightsout-sbc.htm

        case RFNS:      // 06434   // Read function switches
            if (IOT_WRITE_FALLING_EDGE) { IOT_READ_AC(); }
            else
            if (IOT_READ_FALLING_EDGE)
            {
              if (outputDEBUG || outputINFO) { Serial.println("... IOT RFNS : Missing"); };
              xDATA_DIR_OUT();
              SET_DATA_OUT(0x000);
            }
            break;

        /**************************************************************************************************************/
  //      case 06550:     // Type AA01A DAC
  //      case 06551:
  //      case 06552:
  //      case 06553:
  //      case 06554:
  //      case 06555:
  //      case 06556:
  //      case 06557:
  //          break;
        /**************************************************************************************************************/
        default:
            if (outputERROR)
            {
              Serial.print("\n**************\n");
              Serial.print("UNKNOWN IOT - "); Serial.print(IOT_LATCHED, OCT); Serial.println();
              Serial.print("@ "); Serial.print(ADDR_LATCHED_ROM, OCT); Serial.println();
              Serial.print("**************\n");
            }
            // while(1);
            break;
      }
    }

    // Reference/Notes from SBC6120 GAL source code:
    // used to figure out when to drive C0,C1,etc. during
    // which IOT.

    /*   This GAL is repsonsible for driving the C0, C1 and BYTE READ L  */
    /* signals for ALL IOTs, even those that are otherwise handled by IOT */
    /* GAL #2.  Remember that asserting C0 causes the AC to be cleared (for */
    /* either a WRITE or a READ operation), and asserting C1 causes a READ  */
    /* (i.e. device -> AC) transfer to occur.  Asserting BYTE READ L drives */
    /* DX[0:3] low for READ operations from 8 bit devices.  Note that the */
    /* 6120 always samples C0/C1 during the WRITE part of the IOT cycle,  */
    /* regardless of the actual direction of data transfer!     */
    // C0 =        WRITE & (KCC # KRB # IOT_647X # LDAR);
    // C1 =        WRITE & (KRS # KRB # (IOT_647X & !MA9));
    // BYTE_READ = READ  & (KRS # KRB # (IOT_647X & !MA9));

  }
  else
  {
    if ( digitalReadFast(uP_IOCLR_N) && digitalReadFast(uP_RESET_N))
    {
      // reset IOT_IN_IOCLEAR when both IOCLR and RESET are deasserted.
      // b/c  during reset, IOCLR is asserted along w/ LXDAR.
      //      but at some point IOCLR is deasserted while RESET stays asserted.
      //      this will prevent re-triggering the IOT_RESET DEVICES.

      IOT_IN_IOCLEAR  = false;
      first_time      = true;
    }
  }

  // Assert interrupt if keyboard or printer activity.
  digitalWriteFast(uP_INTREQ_N, !( KEYBOARD_FLAG || PRINTER_FLAG || DASP) );

  // ##################################################
  // Save current state as previous so we can do edge
  // detection on next CLK=LOW
  // ##################################################
  prev_uP_LXMAR_N   = digitalReadFast(uP_LXMAR_N);
  prev_uP_LXDAR_N   = digitalReadFast(uP_LXDAR_N);
  prev_uP_LXPAR_N   = digitalReadFast(uP_LXPAR_N);
  // unused prev_uP_MEMSEL_N  = digitalReadFast(uP_MEMSEL_N);
  prev_uP_READ_N    = digitalReadFast(uP_READ_N);
  prev_uP_WRITE_N   = digitalReadFast(uP_WRITE_N);
  // unused prev_uP_IOCLR_N   = digitalReadFast(uP_IOCLR_N);
  prev_uP_DATAF_N   = digitalReadFast(uP_DATAF_N);

  // unused prev_uP_INTGNT_N  = digitalReadFast(uP_INTGNT_N);
  // unused prev_uP_SKIP_N    = digitalReadFast(uP_SKIP_N);

  // unused prev_uP_EMA2      = digitalReadFast(uP_EMA2);
  // unused prev_uP_C1N_EMA1  = digitalReadFast(uP_C1N_EMA1);
  // unused prev_uP_C0N_EMA0  = digitalReadFast(uP_C0N_EMA0);

  prev_ADDR         = xADDR_IN();         // Latch C0,C1,EMA2,DX0...DX11.
  prev_DX           = prev_ADDR & 0x0FFF; // keep only DX0..DX11 for data.

  // Save for read/write handling.
  prev_ROM_READ   = ROM_READ;
  prev_RAM_READ   = RAM_READ;
  prev_RAM_WRITE  = RAM_WRITE;

#if (outputDEBUG)
    delay(SLOW_MODE);
#endif

#if (outputINFO)
    delayMicroseconds(150);
#endif

  // Disable C0,C1,SKIP# gates if out of LXDAR
  if (LXDAR && WRITE)
  {
    // don't touch drive_C0/C1_SKIP yet.
  }
  else
  {
    // Out of IOT, do not drive C0/C1 anymore.
    drive_C0_N(HIGH);
    drive_C1_N(HIGH);
    drive_SKIP_N(HIGH);
  }

  // Tri-state DX if 6120 is not reading.
  if (
      !(ROM_CE && READ) &&        // not reading from ROM
      !(RAM_CS && READ) &&        // not reading from RAM
      !(LXDAR && READ)            // not reading from IOT
     )
  {
    xDATA_DIR_IN();
  }

  // ##################################################
  // CLOCK = HIGH
  // ##################################################
  CLK_HIGH();
  DELAY_FACTOR_H();

  // ##################################################
  // Cycle Count
  // ##################################################
  // clock_cycle_count++;

  // if (outputDEBUG)
  //   delay(SLOW_MODE);
}


////////////////////////////////////////////////////////////////////
// Serial Event
////////////////////////////////////////////////////////////////////

void serialEvent6120()
{
  const byte TRANSMIT_DELAY = 10;     // Fixes missing chars in Arduino IDE
  static byte tx_delay = TRANSMIT_DELAY;

  // RX_BUF            = 0;
  // KEYBOARD_FLAG     = false;      // 1: 6120 needs to read a char from KEYBOARD_BUF. Generate interrupt when set.
  // KEYBOARD_BUF_READ = true;       // arduio printed RX_BUF -- combine w/ KEYBOARD_FLAG.

  if (tx_delay > 0)
    tx_delay--;
  else
  if ( Serial.available() )
  {
    // if (Serial.peek() == 'd') {outputDEBUG = true;  Serial.read(); Serial.read(); Serial.read();};
    // if (Serial.peek() == 'D') {outputDEBUG = false; Serial.read(); Serial.read(); Serial.read();};

    // TODO: From Randy M.
    //       Check if monitor code is ok w/ this.
    //       or we leave as is. 
    // RX_BUF = topper();        // <string.h>
    // RX_BUF += 0200;

    if (KEYBOARD_BUF_READ)
    {
      RX_BUF            = Serial.read();
      if (1) // PDP-8 history
      {
        RX_BUF          = toupper( RX_BUF );
        RX_BUF         += 0200;
      }
      KEYBOARD_FLAG     = true;
      KEYBOARD_BUF_READ = false;

      if (outputDEBUG || outputINFO) { Serial.print("... serialEvent6120 : Serial.read() = 0x"); Serial.println(RX_BUF, HEX); };

      // delay before sending next character from teensy
      tx_delay = TRANSMIT_DELAY;
    }
  }
}


void k6120_sbc6120_init()
{

// If you enable this, it will do a simple Hello world routine.
// One can use this to write their simple assembly code.

// #define HELLO_WORLD_STRING
#ifdef HELLO_WORLD_STRING

  // Switch to Map2 to put RAM instead of ROM.  <<=== !!!!

  MAP0 = false;
  MAP1 = false;
  MAP2 = true;
  MAP3 = false;

  // Load Hello World into RAM
  //
  // 07000,    //  0: NOP             ; gets overwritten to 07777 by 6120 upon reset.
  // 07300,    //  0: HELLO, CLA CLL
  // 01410,    //  1: TAD I Z STPTR
  // 07450,    //  2: SNA
  // 07402,    //  3: HLT
  // 06046,    //  4: TLS
  // 06041,    //  5: TSF
  // 05005,    //  6: JMP .-1
  // 05000,    //  7: JMP HELLO
  // 00010,    // dummy byte for stupid reason
  // 00310,    // 10: H
  // 00345,    // 11: E
  // 00354,    // 12: L
  // 00354,    // 13: L
  // 00357,    // 14: O
  // 00254,    // 15: ,
  // 00240,    // 16: SPACE
  // 00367,    // 17: W
  // 00357,    // 20: O
  // 00362,    // 21: R
  // 00354,    // 22: L
  // 00344,    // 23: D
  // 00241,    // 24: !
  // 00000,    // 25: 0x00

  word a, d;

  a = 0107777; d = 07000; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);      // NOP @ 7777
  a = 0007777; d = 07000; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);      // NOP @ 7777

  a = 0100000; d = 07777; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);      // gets overwriten to 07777 by 6120
  a = 0100001; d = 07300; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0100002; d = 01411; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0100003; d = 07450; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0100004; d = 07402; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0100005; d = 06046; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0100006; d = 06041; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0100007; d = 05005; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0100010; d = 05001; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);

  a = 0100011; d = 00011; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0000011; d = 00011; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  // SBC6120 Memory map differentiates DIRECT vs INDIRECT accesses :(
  //
  a = 0000012; d = 00310; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0000013; d = 00345; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0000014; d = 00354; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0000015; d = 00354; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0000016; d = 00357; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0000017; d = 00254; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0000020; d = 00240; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0000021; d = 00367; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0000022; d = 00357; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0000023; d = 00362; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0000024; d = 00354; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0000025; d = 00344; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0000026; d = 00241; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);
  a = 0000027; d = 00000; RAM_H[ a ] = (d >> 6) & 0x3F; RAM_L[ a ] = (d & 0x3F);

#endif

  // OS8 bootstrap
  // 2024-0403: work-in-progress.
  //            DMAR & DMAW not implemented yet.

	// short dms[] = {
	// 	06603,
	// 	06622,
	// 	05201,
	// 	05604,
	// 	07600,
	// };

	// short os8[] = {
	// 	07600,
	// 	06603,
	// 	06622,
	// 	05352,
	// 	05752,
	// };

	// mem[07750] = 07576;
	// mem[07751] = 07576;
	// memcpy(&mem[0200], dms, sizeof(dms));
	// memcpy(&mem[07750], os8, sizeof(os8));
	// mem[030] = 06743;
	// mem[031] = 05031;

  write_memory(0007750, 07576);
  write_memory(0007751, 07576);

  write_memory(0000200, 06603);
  write_memory(0000201, 06622);
  write_memory(0000202, 05201);
  write_memory(0000203, 05604);
  write_memory(0000204, 07600);

  write_memory(0007750, 07600);
  write_memory(0007751, 06603);
  write_memory(0007752, 06622);
  write_memory(0007753, 05352);
  write_memory(0007754, 05752);

  write_memory(0000030, 06743);
  write_memory(0000031, 05031);


  // Above code samples are from 
  // David Betz (https://github.com/dbetz/pico_pdp8)
  // Ian Schofield (https://groups.google.com/g/pidp-8/c/r1a_KFR5VEQ)

  // MIT License

  // Copyright (c) 2022 David Betz

  // Permission is hereby granted, free of charge, to any person obtaining a copy
  // of this software and associated documentation files (the "Software"), to deal
  // in the Software without restriction, including without limitation the rights
  // to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  // copies of the Software, and to permit persons to whom the Software is
  // furnished to do so, subject to the following conditions:

  // The above copyright notice and this permission notice shall be included in all
  // copies or substantial portions of the Software.

  // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  // IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  // FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  // AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  // LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  // OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  // SOFTWARE.


}


////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////

void setup()
{
  Serial.begin(0);
  while (!Serial);

  Serial.write(27);       // ESC command
  Serial.print("[2J");    // clear screen command
  Serial.write(27);
  Serial.print("[H");
  Serial.println("\n");
  Serial.println("Configuration:");
  Serial.println("==============");
  Serial.println("");
  Serial.println("=======================================================");
  Serial.println("> SBC6120");
  Serial.println(">  by SpareTimeGizmos");
  Serial.println("> ");
  Serial.println("> HD-6120 based SBC.");
  Serial.println("> Runs standard DEC paper tape software");
  Serial.println("> Copyright (C) 2001-2003 by Spare Time Gizmos.");
  Serial.println(">           Made possible thanks to Robert Armstrong"); 
  Serial.println("> ");
  Serial.println("> OS8 Disk image by Steve Gibson, https://www.grc.com");
  Serial.println("> OS8 Disk image by Will Sowerbutts (2017-12-28)");
  Serial.println("=======================================================");
  Serial.println("");


  // Check for disk image on SDCard
  SD_Card_Present = false;
  if (SD.begin(BUILTIN_SDCARD))
  {
    Serial.println("SDCard: Card present!");
    SD_Card_Present = true;
    ide_check_for_sdimage();
    // dsk_init();    // 2024-0403: wip.

    Serial.println();
    Serial.println("=== Useful OS8 commands ================");
    Serial.println("B                   Boot into OS8");
    Serial.println("SET TTY NO PAUSE    Disable pause in OS8");
    Serial.println("DIR SYS:            show drive SYS:");
    Serial.println("DIR IDA1:           show drive IDA1:");
    Serial.println("BASIC               Start BASIC");
    Serial.println("========================================");
    Serial.println();    
    delay(1000);
  }

  Serial.write("\n");

  // Initialize processor GPIO's
  uP_init();
  k6120_sbc6120_init();
  disable_FORCE();

  KEYBOARD_FLAG = false;
  Serial.flush();

  // Reset processor
  uP_assert_reset();
  Serial.println("RESET=1");

  for(int i=0;i<64;i++)
  {
    cpu_tick();
    Serial.print(".");
  }

  uP_release_reset();
  Serial.println();
  Serial.println("RESET=0\n");
}

////////////////////////////////////////////////////////////////////
// Loop()
////////////////////////////////////////////////////////////////////

void loop()
{
  byte i = 0;

  // Loop forever
  //
  while(1)
  {
    cpu_tick();

    // Check serial events once every 256 cycle b/c Serial.Available() is expensive.
    i--;
    if (i == 0) { serialEvent6120(); Serial.flush(); }
  }
}
