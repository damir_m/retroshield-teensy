#ifndef _RK8E_IOT_H
#define _RK8E_IOT_H

  // Code samples are from 
  // David Betz (https://github.com/dbetz/pico_pdp8)
  // Ian Schofield (https://groups.google.com/g/pidp-8/c/r1a_KFR5VEQ)

  // MIT License

  // Copyright (c) 2022 David Betz

  // Permission is hereby granted, free of charge, to any person obtaining a copy
  // of this software and associated documentation files (the "Software"), to deal
  // in the Software without restriction, including without limitation the rights
  // to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  // copies of the Software, and to permit persons to whom the Software is
  // furnished to do so, subject to the following conditions:

  // The above copyright notice and this permission notice shall be included in all
  // copies or substantial portions of the Software.

  // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  // IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  // FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  // AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  // LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  // OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  // SOFTWARE.



static short rkdn, rkcmd, rkca, rkwc;
static int rkda;
static uint8_t  *p;
#if 0
static File rk05;
#endif


void rk8e_iot()
{
    //      printf("Acc:%04o IOT:%04o\n",acc,xmd);
    switch (inst & 7)
    {
    case 0:
        break;
    case 1:
        if (rkdn) {
            pc++;
            rkdn = 0;
        }
        break;
    case 2:
        acc &= 010000;
        rkdn = 0;
        break;
    case 3:
        rkda = acc & 07777;
        //
        // OS/8 Scheme. 2 virtual drives per physical drive
        // Regions start at 0 and 6260 (octal).
        //
        acc &= 010000;
        if (rkcmd & 6) {
            //printf("Unit error\n");
            return;
        }
        switch (rkcmd & 07000)
        {
        case 0:
        case 01000:
            rkca |= (rkcmd & 070) << 9;
            rkwc = (rkcmd & 0100) ? 128 : 256;
            rkda |= (rkcmd & 1) ? 4096 : 0;
            //f_lseek(&rk05, rkda * 512);
#if 0
            rk05.seek(rkda * 512, SeekSet);
#endif
            p = (uint8_t *)&mem[rkca];
            //f_read(&rk05, p, rkwc * 2, &bcnt);
#if 0
            tmp = rk05.read(p, rkwc * 2);
#endif
            //Serial.printf("Read Mem:%04o Dsk:%04o Len:%d\r\n", rkca, rkda, tmp);
            rkca = (rkca + rkwc) & 07777;
            rkdn++;
            // printf(".");
            break;
        case 04000:
        case 05000:
            rkca |= (rkcmd & 070) << 9;
            rkwc = (rkcmd & 0100) ? 128 : 256;
            rkda |= (rkcmd & 1) ? 4096 : 0;
            //printf("Write Mem:%04o Dsk:%04o\n",rkca,rkda);
            //fseek(rk05,rkda*512,SEEK_SET);
            //f_lseek(&rk05, rkda * 512);
#if 0
            rk05.seek(rkda * 512, SeekSet);
#endif
            p = (uint8_t *)&mem[rkca];
            //fwrite(p,2,rkwc,rk05);
            //f_write(&rk05, p, rkwc * 2, &bcnt);
#if 0
            tmp = rk05.write(p, rkwc * 2);
#endif
            //rk05.flush();
            //Serial.printf("Write Mem:%04o Dsk:%04o Len:%d\r\n", rkca, rkda, tmp);
            rkca = (rkca + rkwc) & 07777;
            rkdn++;
            break;
        case 02000:
            break;
        case 03000:
            if (rkcmd & 0200) rkdn++;
            break;
        }
        break;
    case 4:
        rkca = acc & 07777;
        acc &= 010000;
        break;
    case 5:
        //        acc=(acc&010000)|(rkdn?04000:0);
        acc = (acc & 010000) | 04000;
        break;
    case 6:
        rkcmd = acc & 07777;
        acc &= 010000;
        break;
    case 7:
        //printf("Not allowed...RK8E\n");
        break;
    }
}


#endif
