////////////////////////////////////////////////////////////
// Teensy-to-RetroShield Adapter SDCard Example
//   based on Arduino File/Examples/SD/CardInfo
//
// This program performs simple file write/read operations.
// 
// 2020/05/01
// Version 0.1
//
// Credits to:
//   created  28 Mar 2011
//     by Limor Fried 
//   modified 9 Apr 2012
//     by Tom Igoe 
//   modified 1 May 2020
//     by Erturk Kocalar
//
////////////////////////////////////////////////////////////

#include <SD.h>
#include <SPI.h>

File myFile;

// Teensy 3.5 & 3.6 on-board: BUILTIN_SDCARD
const int chipSelect = BUILTIN_SDCARD;

void setup()
{

  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  //////////////////////////////////////////////////
  Serial.print("Starting SD card...");

  if (!SD.begin(chipSelect)) {
    Serial.println("...failed!");
    return;
  }
  Serial.println("...done");

  //////////////////////////////////////////////////
  if (SD.exists("example.txt")) {
    Serial.println("[1] example.txt exists.");
  }
  else {
    Serial.println("[1] example.txt doesn't exist.");
  }

  //////////////////////////////////////////////////
  // open a new file and immediately close it:
  Serial.println("[2] Creating example.txt...");
  myFile = SD.open("example.txt", FILE_WRITE);
  myFile.write("RetroShield says hi!\n");
  myFile.close();

  //////////////////////////////////////////////////
  // Check to see if the file exists: 
  if (SD.exists("example.txt")) {
    Serial.println("[3] example.txt exists.");
  }
  else {
    Serial.println("[3] ERROR: example.txt doesn't exist.");  
  }

  //////////////////////////////////////////////////
  // Read file contents
  myFile = SD.open("example.txt");
  if (myFile) {
    Serial.println("[4] File dump:");
    while (myFile.available()) {
      Serial.write(myFile.read());
    }
    myFile.close();
  }  
  // if the file isn't open, pop up an error:
  else {
    Serial.println("[4] ERROR: can not open example.txt");
  } 

  //////////////////////////////////////////////////
  // delete the file:
  Serial.println("[5] Removing example.txt...");
  SD.remove("example.txt");

  //////////////////////////////////////////////////
  if (SD.exists("example.txt")){ 
    Serial.println("[6] ERROR: example.txt exists.");
  }
  else {
    Serial.println("[6] example.txt doesn't exist.");  
  }
}

void loop()
{
  // nothing happens after setup finishes.
}
