////////////////////////////////////////////////////////////////////
// RetroShield 6809 for Teensy 3.5
// Simon6809
//
// 2019/01/23
// Version 1.0

// The MIT License (MIT)

// Copyright (c) 2019 Erturk Kocalar, 8Bitforce.com

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// Date         Comments                                            Author
// -----------------------------------------------------------------------------
// 09/29/2019   Bring-up on Teensy 3.5.                             Erturk
// 01/11/2024   Add support for Teensy 4.1                          Erturk


////////////////////////////////////////////////////////////////////
// Options
//   outputDEBUG: Print memory access debugging messages.
////////////////////////////////////////////////////////////////////
#define outputDEBUG     0

////////////////////////////////////////////////////////////////////
// BOARD DEFINITIONS
////////////////////////////////////////////////////////////////////

#include "memorymap.h"      // Memory Map (ROM, RAM, PERIPHERALS)
#include "portmap.h"        // Pin mapping to cpu
#include "setuphold.h"      // Delays required to meet setup/hold

unsigned long clock_cycle_count;

void uP_assert_reset()
{
  // Drive RESET conditions
  digitalWriteFast(uP_RESET_N,  LOW);
  digitalWriteFast(uP_FIRQ_N,   HIGH);
  digitalWriteFast(uP_GPIO,     LOW);
  digitalWriteFast(uP_IRQ_N,    HIGH);
  digitalWriteFast(uP_NMI_N,    HIGH);
}

void uP_release_reset()
{
  // Drive RESET conditions
  digitalWriteFast(uP_RESET_N, HIGH);
}

void uP_init()
{
  // Set directions for ADDR & DATA Bus.
  configure_PINMODE_ADDR();
  configure_PINMODE_DATA();

  // byte pinTable[] = {
  //   5,21,20,6,8,7,14,2,     // D7..D0
  //   27,26,4,3,38,37,36,35,  // A15..A8
  //   12,11,25,10,9,23,22,15  // A7..A0
  // };
  // for (int i=0; i<24; i++)
  // {
  //   pinMode(pinTable[i],INPUT);
  // } 
  
  pinMode(uP_RESET_N,   OUTPUT);
  pinMode(uP_RW_N,      INPUT);
  pinMode(uP_FIRQ_N,    OUTPUT);
  pinMode(uP_GPIO,      OUTPUT);
  pinMode(uP_IRQ_N,     OUTPUT);
  pinMode(uP_NMI_N,     OUTPUT);
  pinMode(uP_CLK_E,     OUTPUT);
  pinMode(uP_CLK_Q,     OUTPUT);
  
  digitalWriteFast(uP_CLK_E, LOW);
  digitalWriteFast(uP_CLK_Q, LOW);
  uP_assert_reset();

  clock_cycle_count = 0;
}

////////////////////////////////////////////////////////////////////
// Processor Control Loop
////////////////////////////////////////////////////////////////////

word uP_ADDR;
byte uP_DATA;

byte DATA_OUT;
byte DATA_IN;

inline __attribute__((always_inline))
void cpu_tick()
{    

  //////////////////////////////////////////////////////////////
  CLK_Q_HIGH;   DELAY_FACTOR_QH();

  CLK_E_HIGH;   DELAY_FACTOR_EH();   
  
  uP_ADDR = ADDR();

  CLK_Q_LOW;    DELAY_FACTOR_QL();

  //////////////////////////////////////////////////////////////
  if (STATE_RW_N)      // Check R/W
  // R/W = high = READ?
  {
    // DATA_DIR = DIR_OUT;
    xDATA_DIR_OUT();

    // ROM?
    if ( (ROM_START <= uP_ADDR) && (uP_ADDR <= ROM_END) )
      DATA_OUT = ROM[(uP_ADDR - ROM_START)];
    else 
    // RAM?
    if ( (RAM_START <= uP_ADDR) && (uP_ADDR <= RAM_END) )
      DATA_OUT = RAM[uP_ADDR - RAM_START];
    else
    // FTDI?
    if (uP_ADDR == 0xD000)
      DATA_OUT = FTDI_Read();
    else
      DATA_OUT = 0xFF;

    SET_DATA_OUT( DATA_OUT );     // Start driving the databus out
    DELAY_FOR_BUFFER();           // Let level shifter stabilize.         

#if outputDEBUG
    char tmp[40];
    sprintf(tmp, "-- A=%0.4X D=%0.2X\n", uP_ADDR, DATA_OUT);
    Serial.write(tmp);
#endif

  } 
  else 
  //////////////////////////////////////////////////////////////
  // R/W = low = WRITE?
  {
    DATA_IN = xDATA_IN();
    
    // RAM?
    if ( (RAM_START <= uP_ADDR) && (uP_ADDR <= RAM_END) )
      RAM[uP_ADDR - RAM_START] = DATA_IN;
    else
    // FTDI?
    if (uP_ADDR == 0xD000)
      Serial.write(DATA_IN);

#if outputDEBUG
    char tmp[40];
    sprintf(tmp, "WR A=%0.4X D=%0.2X\n", uP_ADDR, DATA_IN);
    Serial.write(tmp);
#endif
  }


  //////////////////////////////////////////////////////////////
  // cycle complete
  CLK_E_LOW;    DELAY_FACTOR_EL(); 
  
  xDATA_DIR_IN();

#if (outputDEBUG)  
  clock_cycle_count ++;
#endif
}

////////////////////////////////////////////////////////////////////
// Serial Functions
////////////////////////////////////////////////////////////////////

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so any delay inside loop will delay
 response.  
 Warning: Multiple bytes of data may be available.
 */
 
inline __attribute__((always_inline))
void serialEvent6809() 
{
  const byte TRANSMIT_DELAY = 10;
  static byte tx_delay = TRANSMIT_DELAY;

  if (tx_delay > 0)
    tx_delay--;
  else
  if (digitalReadFast(uP_FIRQ_N) == LOW)
  {
    // wait if FIRQ is already asserted.
    return;
  }
  else
  // If serial data available, assert FIRQ so process can grab it.
  if (Serial.available())
  {
      digitalWriteFast(uP_FIRQ_N, LOW);
      tx_delay = TRANSMIT_DELAY;
  }
  return;
}

inline __attribute__((always_inline))
byte FTDI_Read()
{
  byte x = Serial.read();
  // If no more characters left, deassert FIRQ to the processor.
  if (Serial.available() == 0)
      digitalWriteFast(uP_FIRQ_N, HIGH);
  return x;
}

// This function is not used by the timer loop
// because it takes too long to call a subrouting.
inline __attribute__((always_inline))
void FTDI_Write(byte x)
{
  Serial.write(x);
}

////////////////////////////////////////////////////////////////////
// Interrupt Functions
// 
// interruptEventNMI - generates NMI pulse periodically
////////////////////////////////////////////////////////////////////
#define CONST_NMI_TIMER_STARTUP_DELAY 1000000
#define CONST_NMI_TIMER_CYCLE_COUNT 1000
#define NMI_PULSE_WIDTH 10

inline __attribute__((always_inline))
void interruptEventNMI()
{
  // Set initially to start-up delay so NMI does not
  // fire immediately before SW has time to setup
  // any RAM pointers (if used)
  static long cycleCount = CONST_NMI_TIMER_STARTUP_DELAY;

  cycleCount--;
  if (cycleCount == 0)
  {
    if (digitalReadFast(uP_NMI_N) == true)     // NMI# was high
    {
      // High -> Low edge
      digitalWriteFast(uP_NMI_N, LOW);
      cycleCount = NMI_PULSE_WIDTH;
      // Serial.print("*");
    } else {                             // NMI# was low
      // Low -> High edge
      digitalWriteFast(uP_NMI_N, HIGH);
      cycleCount = CONST_NMI_TIMER_CYCLE_COUNT;      
      // Serial.print("-");
    }
  }
}

////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////

void setup() 
{
  Serial.begin(115200);
  while (!Serial);

  Serial.write(27);       // ESC command
  Serial.print("[2J");    // clear screen command
  Serial.write(27);
  Serial.print("[H");
  Serial.println("\n");
  Serial.println("Configuration:");
  Serial.println("==============");
  print_teensy_version();
  Serial.print("Debug:      "); Serial.println(outputDEBUG, HEX);
  Serial.print("SRAM Size:  "); Serial.print(RAM_END - RAM_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("SRAM_START: 0x"); Serial.println(RAM_START, HEX); 
  Serial.print("SRAM_END:   0x"); Serial.println(RAM_END, HEX); 
  Serial.println("=======================");
  Serial.println(" press h for help");
  Serial.println("=======================");
  

  uP_init();
  
  // Reset processor
  uP_assert_reset();
  for(int i=0;i<25;i++) cpu_tick();  
  uP_release_reset();

  Serial.println("\n");
}

////////////////////////////////////////////////////////////////////
// Loop
////////////////////////////////////////////////////////////////////

void loop()
{
  byte i = 0;

  // Loop forever
  //  
  while(1)
  {
    cpu_tick();

    // Check serial events but not every cycle.
    // Watch out for clock mismatch (cpu tick vs serialEvent counters are /128)
    i++;
    if (i == 0)  serialEvent6809();
    if (i == 0)  Serial.flush();
  }
}
